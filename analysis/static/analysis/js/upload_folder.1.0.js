// Generate 32 char random uuid
function gen_uuid() {
    var uuid = ""
    for (var i=0; i < 32; i++) {
        uuid += Math.floor(Math.random() * 16).toString(16);
    }
    return uuid
}

// Add upload progress for multipart forms.
$(function() {
    $('#upload_folder').submit(function(){
        // Prevent multiple submits
        if ($.data(this, 'submitted')) return false;

        var freq = 100; // freqency of update in ms
        var uuid = gen_uuid(); // id for this upload so we can fetch progress info.
        var progress_url = '/upload_progress/'; // ajax view serving progress info

        // Append X-Progress-ID uuid form action
        this.action += (this.action.indexOf('?') == -1 ? '?' : '&') + 'X-Progress-ID=' + uuid;

        var $progress = $('<div class="container"><div id="upload-progress" class="upload-progress"><b>Don\'t close or refresh the page until you will get a message on the end of the uploading.</b><br></div>').
            appendTo(document.body).append('<div class="progress-container w3-grey w3-round"><div class="progress-info w3-round w3-blue w3-padding" style="color:black; width:0%; white-space: nowrap">uploading 0%</div></div></div>');

        // progress bar position
        $progress.css({
            position: 'absolute',
            left: '50%', marginLeft: 0-($progress.width()/2), bottom: '20%'
        }).show();

        // Update progress bar
        function update_progress_info() {
            $progress.show();
            $.getJSON(progress_url, {'X-Progress-ID': uuid}, function(data, status){
                if (data) {
                    var progress = 0;
                    if (parseInt(data.uploaded) >= parseInt(data.length)){
                        progress = 1;
                    } else {
                        progress = parseInt(data.uploaded) / parseInt(data.length);
                    }
                    var width = $progress.find('.progress-container').width()
                    var progress_width = width * progress;
                    $progress.find('.progress-info').width(progress_width);
                    $progress.find('.progress-info').text('uploading ' + parseInt(progress*100) + '%');
                }
                window.setTimeout(update_progress_info, freq);
            });
        };
        window.setTimeout(update_progress_info, freq);

        $.data(this, 'submitted', true); // mark form as submitted.
    });
});


