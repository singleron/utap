function confirm_delete(job_name, pipeline) {
    var result = confirm("Want to delete the analysis "+ job_name + "?\nIt will delete the output folder form wexac server.");
    if (result) {
        $.ajax({
            url: '/ajax/delete_analysis/',
            dataType: 'json',
            data: {
                'job_name': job_name,
                'pipeline': pipeline
            },
            success: function (data_out_get) {
                var ok_delete = confirm("This action will delete these folder/s:\n" + data_out_get.deleted);
                if (ok_delete){
                    $.ajax({
                        url: '/ajax/delete_analysis/',
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'job_name': job_name,
                            'pipeline': pipeline
                        },
                        success: function (data_out_post) {
                            alert('The process of deletion the folder/s of the analysis ' + job_name + ' is running.');
                            window.location = data_out_post.redirect_url;
                        },
                        error: function(data_out_post) {
                            alert('Cannot delete the analysis ' + job_name + ' ' + data_out_post.deleted + 'Please try again.');
                        }
                    });
                }
            },
            error: function(data_out_get) {
                var json = JSON.parse(data_out_get.responseText);
                alert("Failed to delete the analysis: " + job_name + ". " + json.message);
            }
        });
    }
}
