//import this file as ES6 "module" - <script type="module" src="get_samples.js"></script>

export function get_samples_list(path, pipeline, is_submit) {//if run_deseq changed to 'yes'
    var ajax_results = []
    if (!is_submit){ //regular pipeline - not submit
        $.ajax({
            url: '/ajax/samples_list/',
            dataType: 'json',
            data: {
                'root_dir': path,
                'pipeline': pipeline
            },
            async: false, //Js waits until the ajax ended
            success: function(data_out) {
                ajax_results = [true, data_out];
            },
            error: function(data_out, textStatus, errorThrown) {
                ajax_results = [false, data_out, textStatus, errorThrown]; //textStatus, errorThrown not in use yet
            },
        });
    } else { // It is submit pipeline (the input folder is on stefan server)
        var input = $("#id_input_folder").val();
        var run_id = input.substr(input.lastIndexOf('/') + 1);
        $.ajax({
            url: '/ajax/remote_samples_list/',
            dataType: 'json',
            data: {
                'run_id': run_id
            },
            async: false, //Js waits until the ajax ended
            success: function(data_out) {
                ajax_results = [true, data_out];
            },
            error: function(data_out, textStatus, errorThrown) {
                ajax_results = [false, data_out, textStatus, errorThrown]; //textStatus, errorThrown not in use yet
            },
        });
    }
    return ajax_results;
};

//export function basename(path) {
//    return path.replace(/\\/g,'/').replace( /.*\//, '' );
//}

export function dirname(path) {
    return path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');
}

export function free_space() {
    $.ajax({
        url: '/ajax/free_space/',
        dataType: 'json',
        data: {
            'folder': $("#id_output_folder").val()
        },
        success: function (data_out) {},
        error: function(data_out, textStatus, errorThrown) {
            var json = JSON.parse(data_out.responseText);
            alert(json.message);//message with warnning on disk space
        }
    });
}

