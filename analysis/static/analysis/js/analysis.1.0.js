var samplesheetData = [['', ''],['', ''],['', ''],['', ''],];

$(document).ready(function(){
    document.getElementById("Run").disabled = false;
    $('#samplesheet_table').jexcel({
        data:samplesheetData,
        colHeaders: ['Sample', 'Barcode'],
        colWidths: [ 300, 80],
        columns: [
            { type: 'text' },
            { type: 'text' },
        ]
    });

});

function enable_select_fields() {
    $('select:disabled, input:disabled').each(function () {
       $(this).removeAttr('disabled');
    });
}

function submit_disable_button() {
    if ( document.getElementById("samplesheet_table") ){
        document.getElementById("id_samplesheet").value = samplesheetData;
    }
    enable_select_fields();
    if ($('#form_analysis')[0].checkValidity()){//deny to press again on run_analysis button.
        document.getElementById("Run").disabled = true;
    }
    $('#hidden-submit').click();
};

