$(function(){
    <!--document.getElementById("invalid_browser").innerHTML = "The website is under construction, please don't run analysis";-->
    if (typeof window.fetch === "undefined" && window.navigator.userAgent.indexOf('Chrome/') === -1 && window.navigator.userAgent.indexOf('Firefox/') === -1) {
        document.getElementById("invalid_browser").innerHTML = 'Warnning: UTAP - NGS PIPELINES only runs with Chrome version 43 and up or Firefox browser';
    }
});