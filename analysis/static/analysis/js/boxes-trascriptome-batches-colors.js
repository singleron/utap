//These functions cannot be in transcriptome boxes, because they called from html that cannot access to functions in ES6 module
var MAX_BATCHES = 12;//Maximum number of batches.

function changeColor(colorButton, batchNumber) {
    if ($(".selectedcolor_nocolor").length ) {
        $(".selectedcolor_nocolor").attr("class", "selectedcolor_" + colorButton);//changes class
        var updatedSamples = $(".selectedcolor_" + colorButton).map(function() {
            return $(this).val();
        }).get();
        $("#batch"+batchNumber+"-samples").val(updatedSamples.join(','));
        for (var i = 1; i<=MAX_BATCHES; i++) {
            if (i != parseInt(batchNumber)) {
                for (var j=0; j<updatedSamples.length; j++) {
                    oldSamples = $("#batch"+i+"-samples").val().split(',');
                    var index = $.inArray(updatedSamples[j].toString(), oldSamples)
                    if (index != -1){
                        oldSamples.splice(index, 1);
                        $("#batch"+i+"-samples").val(oldSamples.join(','));
                    }
                }
            }
        }
    }
    $(".added").find("option:selected").removeAttr("selected");
    $(".added").find(".batch").blur();

};


function colorSelected(select) {
    $(".selectedcolor_nocolor").removeClass("selectedcolor_nocolor");
    $(".added").find("option:selected").addClass("selectedcolor_nocolor")
//    for (var i = 0; i < select.length; i++) {
//        if (select.options[i].selected && $("#batch_effect").val() == "Remove Batch Effect") {
//            select.options[i].className += " selectedcolor_nocolor";
//        }
//    }
};

