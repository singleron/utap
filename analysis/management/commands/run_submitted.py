import datetime
import time

from django.core.management.base import BaseCommand
from analysis.scripts.track_jobs import RunSubmmited


class Command(BaseCommand):
    help = 'Run submitted analyses'

    def check_submitted(self, *args, **options):
        success, failed = RunSubmmited().run_submitted_analyses()
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if success:
            self.stdout.write(self.style.SUCCESS(now + ' Successful to run the submitted: ' + '\n' + '\n'.join(success)))
        if failed:
            self.stdout.write(self.style.ERROR(now + ' Failed to run the submitted: ' + '\n' + '\n'.join(failed)))

    def handle(self, *args, **options):
        while True:
            self.check_submitted()
            time.sleep(60)
