import datetime
import time

from django.core.management.base import BaseCommand

from analysis.scripts.track_jobs import TrackJobs


class Command(BaseCommand):
    help = 'Track after status of jobs and update database'

    def check_status(self):
        success, failed = TrackJobs.update_analysis_db()
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if success:
            self.stdout.write(self.style.SUCCESS(now + ' Runs successfully ended: ' + '\n' + '\n'.join(success)))
        if failed:
            self.stdout.write(self.style.ERROR(now + ' Runs failed: ' + '\n' + '\n'.join(failed)))

    def handle(self, *args, **options):
        while True:
            self.check_status()
            time.sleep(60)