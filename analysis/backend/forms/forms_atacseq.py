"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging

from django import forms
from django.utils.translation import gettext as _
from django.utils.translation import ugettext_lazy as _

from analysis.backend.forms.forms_base import AnalysisBaseForm
from analysis.backend.utils.consts import ATACseqConst
from analysis.models import AtacSeqAnalysis

logger = logging.getLogger(__name__)

from analysis.backend.utils.consts import BASIC_ID, ATACseqConst ,AtacSeqFields


class AtacSeqForm(AnalysisBaseForm):
    commands = ATACseqConst.COMMANDS
    def __init__(self, user_name, *args, **kwargs):
        super(AtacSeqForm, self).__init__(user_name, *args, **kwargs)
        # FileBrowseField override the help_text from Meta class
        self.fields['input_folder'].help_text = _(
            'Folder with fastq files in appropriate structure (see help for details).')
        self.fields['input_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['input_folder'].required = True  # blank=true don't work in FileBrowseField
        # ChainedForeignKey override the genome and annotation form of the Meta class
        self.fields['genome'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['tss_file'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['tss_file'].required = False
        self.get_all_class_fields(AtacSeqFields())

    class Meta(AnalysisBaseForm.Meta):
        model = AtacSeqAnalysis
        fields = ['name', 'pipeline', 'input_folder', 'genome', 'tss_file', 'output_folder', 'email']

    # adapter_on_R1 = forms.CharField(required=True, initial=ATACseqConst.ADAPTER1_INIT,
    #                                 help_text=ATACseqConst.ADAPTER1_HELP,
    #                                 label=ATACseqConst.ADAPTER1_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}))
    #
    # adapter_on_R2 = forms.CharField(required=False, initial=ATACseqConst.ADAPTER2_INIT,
    #                                 help_text=ATACseqConst.ADAPTER2_HELP,
    #                                 label=ATACseqConst.ADAPTER2_LABLE,
    #                                 widget=forms.TextInput(
    #                                     attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80}))
    #
    # treat_vs_control = forms.ChoiceField(required=True,
    #                                      initial=ATACseqConst.TRT_CONT_INIT,
    #                                      choices=ATACseqConst.TRT_CONT_CHOICES,
    #                                      help_text=ATACseqConst.TRT_CONT_HELP,
    #                                      label=ATACseqConst.TRT_CONT_LABEL,
    #                                      widget=forms.Select(
    #                                          attrs={'class': 'btn btn-default', 'form_id': BASIC_ID}))
