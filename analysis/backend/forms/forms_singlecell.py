"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging

from django import forms
from django.utils.translation import ugettext_lazy as _

from analysis.backend.forms.forms_base import AnalysisBaseForm
from analysis.backend.utils.consts import BASIC_ID
from analysis.models import SinglecellAnalysis

logger = logging.getLogger(__name__)


class SinglecellForm(AnalysisBaseForm):
    def __init__(self, user_name, *args, **kwargs):
        super(SinglecellForm, self).__init__(user_name, *args, **kwargs)
        # FileBrowseField override the help_text from Meta class
        self.fields['input_folder'].help_text = _(
            'Folder with fastq files in appropriate structure (see help for details).')
        self.fields['input_folder'].widget.attrs = {'class': 'btn btn-default', 'form_id': BASIC_ID}
        self.fields['input_folder'].required = True  # blank=true don't work in FileBrowseField

    class Meta(AnalysisBaseForm.Meta):
        model = SinglecellAnalysis
        fields = ['name', 'pipeline', 'input_folder', 'cellranger_genome', 'output_folder', 'email']
        AnalysisBaseForm.Meta.widgets['cellranger_genome'] = forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})

