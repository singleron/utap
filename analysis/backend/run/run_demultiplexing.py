"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging
import re
from abc import ABCMeta, abstractmethod

from analysis.backend.run.run_base import RunAnalysis
from analysis.backend.settings import NEXTSEQ_SERVER, NEXTSEQ_USER, BCL_REMOTE_PATH, BCL_REMOTE_PATH_INCPM, \
    BCL_REMOTE_PATH_INCPM_NOVA, DEMULTIPLEXING_SCRIPT, PARAMETERS_DIR, RUN_LOCAL, RUN_LOCAL_HOST_CONDA, \
    BCL2FASTQ_EXE, PIPELINE_URL
from analysis.backend.utils.bcl2fastq import RunParametersReader, CreateSampleSheet
from analysis.backend.utils.consts import DemultConst
from analysis.backend.utils.snakemake import Snakemake

logger = logging.getLogger(__name__)


class RunDemultiplexing(RunAnalysis):
    __metaclass__ = ABCMeta

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunDemultiplexing, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                params_yaml_file, params_json_file, post_params, date, output_dir,
                                                **kwargs)
        self.results_link_ = "%s/demultiplexing_results/" % (PIPELINE_URL)
        self.protocol = self.clean_data_form.protocol
        self.sample_sheet_file = os.path.join(self.user_commands_dir, 'SampleSheet_' + self.job_name)
        self.clean_samplesheet = []
        self.pipeline_commands = []
        if not output_dir:  # for run again in the same output_dir - for now always the class not get output folder for demultiplexing pipelines
            self.output_dir = os.path.join(self.prj_dir, self.job_name + '_demultiplexing')

    @abstractmethod
    def get_prj_dir(self):
        pass

    @abstractmethod
    def save_relevant_parameters(self):
        pass

    @abstractmethod
    def demultiplexing_commands(self):
        pass  # return response (raise Exception or return stdout etc.)

    def send(self):
        self.edit_samplesheet_form()
        self.demultiplexing_commands()
        self.run_commands()

    def edit_samplesheet_form(self):
        error_massage = 'The SampleSheet of run %s is not corrected: missing barcodes or sample name.' % self.job_name
        splited_ss = self.clean_data_form.samplesheet.split(',')
        for samp, barc in zip(splited_ss[::2], splited_ss[1::2]):
            if samp and barc:
                self.clean_samplesheet.append([samp, barc])
                illegal_characters = [i for i in
                                      [" ", "?", "(", ")", "[", "]", ".", "/", "\\", "=", "+", "<", ">", ":", ";", "\"",
                                       "'", "*", "^", "|", "&"] if i in samp]
                if illegal_characters:
                    illegal_characters_error = "The sample name %s contains illegal character/s [%s]" % (
                        samp, ','.join(illegal_characters))
                    logger.error(illegal_characters_error)
                    raise Exception(illegal_characters_error)
                reg = re.compile('^[AGCT]+$')
                if not reg.match(barc):
                    illegal_bar_error = "The barcode of sample %s contains illegal base/s. Barcode can contains only [AGCT] characters" % (
                        samp)
                    logger.error(illegal_bar_error)
                    raise Exception(illegal_bar_error)
            elif not (samp and barc):
                continue
            else:
                logger.error(error_massage)
                raise Exception(error_massage)
        if not self.clean_samplesheet:
            logger.error(error_massage)
            raise Exception(error_massage)

    def run_commands(self):
        snakefile_name = os.path.join(self.user_commands_dir, 'snakefile_%s' % self.job_name)
        if RUN_LOCAL:
            snakemake = Snakemake(self.job_name, self.date, snakefile_name, self.queue, config_file=None, num_jobs=None,
                                  num_cores=1, cluster=False)
        else:
            snakemake = Snakemake(self.job_name, self.date, snakefile_name, self.queue)
        snakemake.create_snakefile(self.pipeline_commands)
        snakemake.prepare_cmd_file()
        if RUN_LOCAL and not RUN_LOCAL_HOST_CONDA:
            snakemake.run_snakefile()
        else:
            snakemake.run_snakefile_ssh()

    def create_samplesheet_file(self, ss_format):
        SampleSheet = CreateSampleSheet(self.sample_sheet_file, ss_format, self.clean_samplesheet)
        SampleSheet.validate_samplesheet()
        SampleSheet.write_samplesheet()


class RunDemultiplexingFastq(RunDemultiplexing):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        # fastq_r1 = self.clean_data_form.fastq_file_R1
        # fastq_r2 = self.clean_data_form.fastq_file_R2  # if self.clean_data_form.fastq_file_R2 else "none"
        # fastq_I = self.clean_data_form.fastq_file_I
        # self.update_kwargs_if_not_exists(kwargs, fastq_r1=fastq_r1, fastq_r2=fastq_r2, fastq_I=fastq_I)
        super(RunDemultiplexingFastq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                     params_yaml_file, params_json_file, post_params, date, output_dir,
                                                     **kwargs)
        if 'fastq_file_R1' in self.clean_data_form.__dict__:
            self.fastq_r1 = self.clean_data_form.fastq_file_R1
        # if self.clean_data_form.fastq_file_R2 else "none"
        if 'fastq_file_R2' in self.clean_data_form.__dict__:
            self.fastq_r2 = self.clean_data_form.fastq_file_R2
        if 'fastq_file_I' in self.clean_data_form.__dict__:
            self.fastq_I = self.clean_data_form.fastq_file_I

    def get_prj_dir(self):
        return os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.output_folder))

    def save_relevant_parameters(self):
        self.relevant_parameters['Fastq_R1'] = self.fastq_r1
        self.relevant_parameters['Fastq_R2'] = self.fastq_r2
        self.relevant_parameters['Fastq_I'] = self.fastq_I
        self.relevant_parameters['Output dir'] = self.output_dir
        self.relevant_parameters['Protocol'] = self.clean_data_form.protocol
        self.relevant_parameters['SampleSheet file'] = self.sample_sheet_file

    def demultiplexing_fastq_cmd(self):
        demultiplexing_cmd = 'mkdir %s; %s --barcodes %s --fastq1 %s --fastq2 %s --fastq3 %s --threshold1 0 --outdir %s --add2header ' % (
            self.output_dir, DEMULTIPLEXING_SCRIPT, self.sample_sheet_file, self.fastq_r1, self.fastq_I,
            self.fastq_r2, self.output_dir)
        if self.protocol == DemultConst.TRUE_SEQ:
            demultiplexing_cmd += ' --no-out-read 2'
        self.pipeline_commands.append([demultiplexing_cmd, 'RunDemultiplexingFastq', 20])

    def demultiplexing_commands(self):
        self.create_samplesheet_file(ss_format=DemultConst.FASTQ_V)
        self.demultiplexing_fastq_cmd()


class RunDemultiplexingBcl(RunDemultiplexingFastq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        # bcl_dir = os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.bcl_files))
        # runinfo_file = os.path.join(bcl_dir, 'RunInfo.xml')
        # self.update_kwargs_if_not_exists(kwargs, bcl_dir=bcl_dir, runinfo_file=runinfo_file)
        super(RunDemultiplexingBcl, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                   params_yaml_file, params_json_file, post_params, date, output_dir,
                                                   **kwargs)
        if 'bcl_files' in self.clean_data_form.__dict__:
            self.bcl_dir = os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.bcl_files))
            self.runinfo_file_ = os.path.join(self.bcl_dir, 'RunInfo.xml')

    def get_prj_dir(self):
        return os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.output_folder))

    def get_reads_type(self):
        reads_details = RunParametersReader().parse(self.runinfo_file_)
        reads_type = []
        for i, read in enumerate(reads_details):
            reads_type.append('i*' if read.is_indexed else 'y*')
        return reads_type

    def save_relevant_parameters(self):
        self.relevant_parameters['Bcl dir'] = self.bcl_dir
        self.relevant_parameters['Output dir'] = self.output_dir
        self.relevant_parameters['Protocol'] = self.clean_data_form.protocol
        self.relevant_parameters['SampleSheet file'] = self.sample_sheet_file

    def bcl2fastq_cmd(self, demultiplexing):
        if demultiplexing:
            if self.protocol == DemultConst.TRUE_SEQ:
                bcl2fastq_cmd = "%s --runfolder-dir %s --processing-threads 8 " \
                                "--loading-threads 4 --writing-threads 4 --mask-short-adapter-reads 1 --output-dir %s " \
                                "--sample-sheet %s --barcode-mismatches 0 --no-lane-splitting" \
                                % (BCL2FASTQ_EXE, self.bcl_dir, self.output_dir, self.sample_sheet_file)
            elif self.protocol == DemultConst.MARS_SEQ:
                bcl2fastq_cmd = "%s --runfolder-dir %s --processing-threads 8 " \
                                "--loading-threads 4 --writing-threads 4 --mask-short-adapter-reads 1 --output-dir %s " \
                                "--sample-sheet %s --barcode-mismatches 0 --no-lane-splitting --use-bases-mask y*,i7y*" \
                                % (BCL2FASTQ_EXE, self.bcl_dir, self.output_dir, self.sample_sheet_file)
        else:
            reads_type = self.get_reads_type()
            num_reads_no_index = reads_type.count('y*')
            num_reads_index = reads_type.count('i*')

            self.fastq_dir = os.path.join(self.prj_dir, self.job_name + '_bcl2fastq')
            self.fastq_r1 = os.path.join(self.fastq_dir, 'Undetermined_S0_R1_001.fastq.gz')
            self.fastq_I = os.path.join(self.fastq_dir, 'Undetermined_S0_I1_001.fastq.gz')
            self.fastq_r2 = "none" if num_reads_no_index == 1 else os.path.join(self.fastq_dir,
                                                                                'Undetermined_S0_R2_001.fastq.gz')
            if num_reads_index == 0:
                self.fastq_I = os.path.join(self.fastq_dir, 'Undetermined_S0_R2_001.fastq.gz')
                self.fastq_r2 = "none"
            bcl2fastq_cmd = "%s --runfolder-dir %s --processing-threads 8 " \
                            "--loading-threads 4 --writing-threads 4 --mask-short-adapter-reads 1 --barcode-mismatches 0 --output-dir %s " \
                            "--use-bases-mask %s --create-fastq-for-index-reads --no-lane-splitting" \
                            % (BCL2FASTQ_EXE, self.bcl_dir, self.fastq_dir, ','.join(reads_type))

        self.pipeline_commands.append([bcl2fastq_cmd, 'RunDemultiplexingBcl', 20])

    def demultiplexing_bcl_cmd(self):
        self.create_samplesheet_file(ss_format=DemultConst.BCL_V)
        self.bcl2fastq_cmd(demultiplexing=True)

    def demultiplexing_commands(self):
        self.demultiplexing_bcl_cmd()


class RunDemultiplexingRunid(RunDemultiplexingBcl):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunDemultiplexingRunid, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                                     params_yaml_file, params_json_file, post_params, date, output_dir,
                                                     **kwargs)
        self.bcl_dir = os.path.join(self.user_base_dir, self.run_id_ + '_' + self.job_name,
                                    self.run_id_ + '_copied_bcl')
        bcl_remote_path = BCL_REMOTE_PATH
        if 'NB501540' in self.run_id_:
            bcl_remote_path = BCL_REMOTE_PATH_INCPM 
        elif 'A00929' in self.run_id_:
            bcl_remote_path = BCL_REMOTE_PATH_INCPM_NOVA
        self.bcl_remote_dir = os.path.join(bcl_remote_path, self.run_id_)  # On stefan server
        self.runinfo_file_ = os.path.join(PARAMETERS_DIR, 'RunInfo_%s.xml' % self.job_name)
        copy_cmd = 'scp %s@%s:%s %s ' % (
            NEXTSEQ_USER, NEXTSEQ_SERVER, os.path.join(self.bcl_remote_dir, 'RunInfo.xml'), self.runinfo_file_)
        logger.info(copy_cmd)
        os.system(copy_cmd)

    def get_prj_dir(self):
        self.run_id_ = self.clean_data_form.run_id.strip()  # before the calling to parent
        return os.path.abspath(
            os.path.join(self.user_base_dir, self.clean_data_form.output_folder, self.run_id_ + '_' + self.job_name))

    def save_relevant_parameters(self):
        self.relevant_parameters['Run id'] = self.run_id_
        self.relevant_parameters['Output dir'] = self.output_dir
        self.relevant_parameters['Protocol'] = self.clean_data_form.protocol
        self.relevant_parameters['SampleSheet file'] = self.sample_sheet_file

    def copy_bcl_files_cmd(self):
        scp_bcl_cmd = "rsync -av %s@%s:%s/* %s " % (NEXTSEQ_USER, NEXTSEQ_SERVER, self.bcl_remote_dir, self.bcl_dir)
        logger.info(scp_bcl_cmd)
        self.pipeline_commands.append([scp_bcl_cmd, 'RunDemultiplexingRunid', 1])

    def demultiplexing_commands(self):
        self.copy_bcl_files_cmd()
        self.demultiplexing_bcl_cmd()
