"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import logging
import os
from abc import ABCMeta, abstractmethod

import yaml

from analysis.backend.run.parsing_post import ParsingPost
from analysis.backend.utils.users import get_user_dir

logger = logging.getLogger(__name__)


class ObjFromDict(object):
    def __init__(self, **entries):
        self.__dict__.update(entries)


class RunAnalysis(object):
    __metaclass__ = ABCMeta

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        self.pipeline = pipeline
        self.request = request
        self.user = username
        self.run_by_user = run_by_user
        self.loggedas_user = request.user.loggedas_username
        # form contains the model fields (this is cleaned_data)
        self.form = form
        self.params_yaml_file = params_yaml_file
        self.params_json_file = params_json_file
        self.queue = queue
        self.output_dir = output_dir
        self.clean_data_form = ObjFromDict(**form)
        self.job_name = job_name
        self.date = date
        # post parameters contains the all fields including non-model fields (this is request.POST)
        self.post_params = ParsingPost.handle_post_params(post_params)
        logger.info(self.post_params)
        self.post_params_obj = ObjFromDict(**(self.post_params))
        self.json_main = {}
        self.relevant_parameters = {}
        self.user_base_dir = get_user_dir(self.user)
        # You must send to kwargs all attributes that this __init__ uses.
        # For example self.input_dir_ that self.get_prj_dir() abstract method uses.
        # Must to update kwargs before the calling to abstract methods (because they uses with the parameters in kwargs).
        self.__dict__.update(kwargs)
        self.prj_dir = self.get_prj_dir()
        self.user_commands_dir = os.path.join(self.prj_dir, 'commands_log')

    @abstractmethod
    def get_prj_dir(self):
        pass

    @abstractmethod
    def save_relevant_parameters(self):
        pass

    @abstractmethod
    def send(self):
        pass

    # Update in kwargs of parent only if the son didn't override the value
    def update_kwargs_if_not_exists(self, kwargs_old, **kwargs_new):
        for k, v in kwargs_new.iteritems():
            if k not in kwargs_old.keys():
                kwargs_old.update({k: v})

    def create_commands_dir(self):
        os.system('mkdir -p %s' % self.user_commands_dir)

    def create_yaml_parameters_file(self, output_file):
        try:
            with open(output_file, 'w') as yaml_file:
                yaml.safe_dump(self.relevant_parameters, yaml_file, default_flow_style=False)
        except Exception as e:
            message = 'Cannot write the yaml parameters file %s because Error: %s' % (self.params_yaml_file, str(e))
            logger.error(message)
            raise Exception(message)

    def send_analysis(self):
        try:
            self.create_commands_dir()
            self.save_relevant_parameters()
            logger.info("Parameters of the analysis are: " + str(self.relevant_parameters))
            self.create_yaml_parameters_file(self.params_yaml_file)
            json = self.create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params,
                                                            self.user,
                                                            self.output_dir)
            response = self.send()  # run the program
            return response, self.results_link_, self.output_dir
        except Exception as e:
            logger.error('Cannot run analysis because the error: %s' % str(e))
            return Exception('Cannot run analysis because the error: %s' % str(e)), self.results_link_, self.output_dir

    def create_json_parameters_file_to_post(self, output_file, form, post_params, user, output_folder):
        form_str = {}
        for key, value in form.items():
            form_str[key] = str(value)
        for key, value in post_params.items():
            form_str[key] = str(value)
        # form_str['run_id'] = os.path.basename(form_str['input_folder'])
        form_str['user'] = user
        form_str['output_folder'] = output_folder
        json_main = json.JSONEncoder().encode(form_str)
        try:
            with open(output_file, 'w') as param_file:
                param_file.write(json_main)
            return json_main
        except Exception as e:
            logger.error('Cannot write parameters file %s because Error: %s' % (output_file, str(e)))
            raise Exception('Cannot write parameters file %s because Error: %s' % (output_file, str(e)))
