"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging
from analysis.backend.forms.forms_transcriptome import TranscriptomeBaseForm
from analysis.backend.forms.forms_transcriptome import RnaSeqForm
from analysis.backend.run.run_ngs_transcriptome import RunTranscriptome
from analysis.backend.settings import SNAKEFILE_RNASEQ, SNAKEFILE_SCRIPTS, \
    SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, RSCRIPT, RESULTS_URL, R_LIB_PATHS, CUTADAPT_EXE, FASTQC_EXE, STAR_EXE, \
    SAMTOOLS_EXE, NGS_PLOT_EXE, HTSEQ_COUNT_EXE, PIPELINE_URL, CONDA_ROOT, GS_EXE, MAX_CORES

logger = logging.getLogger(__name__)

class RunRnaSeq(RunTranscriptome):
    pipeline_name_ = 'RNA-seq'

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunRnaSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                        params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.username = username
        self.results_link_ = '%s/%s/report_output_%s/report.html' % (RESULTS_URL, self.job_name, self.date)
        self.snakefile_source = SNAKEFILE_RNASEQ
        self.config_file = os.path.join(self.output_dir, 'config-rnaseq-%s.yaml' % self.date)
        self.gtf = self.clean_data_form.annotation.path
        self.adapters = self.clean_data_form.adapter_on_R1.lstrip()
        if hasattr(self.clean_data_form, 'adapter_on_R2'):
            self.adapters += "," + self.clean_data_form.adapter_on_R2.lstrip()
        self.stranded = '"' + self.clean_data_form.stranded + '"'
        self.results_dir = os.path.join(self.output_dir, '4_reports')
        # advanced parameters:
        #DEseq
        self.correct_with_FDR_tool =  self.clean_data_form.correct_with_FDR_tool
        self.log2_Fold_Change = self.clean_data_form.log2_Fold_Change
        self.adjusted_P_value = self.clean_data_form.adjusted_P_value
        self.baseMean = self.clean_data_form.baseMean
        self.log2FC = self.clean_data_form.log2_Fold_Change
        #CUTADAPT
        self.additional_Adapter_R1 = self.clean_data_form.aditional_adapter_on_R1
        self.additional_Adapter_R2 = self.clean_data_form.aditional_adapter_on_R2
        self.cutadapt_qvalue = self.clean_data_form.cutadapt_qvalue
        self.cutadapt_min_len = self.clean_data_form.cutadapt_min_len
        self.cutadapt_cut_start = "" if str(self.clean_data_form.cutadapt_cut_start) == 'None' else self.clean_data_form.cutadapt_cut_start
        self.cutadapt_cut_end = "" if str(self.clean_data_form.cutadapt_cut_end) == 'None' else self.clean_data_form.cutadapt_cut_end

    # def save_relevant_parameters(self):
    #     self.relevant_parameters['Input dir'] = self.input_dir
    #     self.relevant_parameters['Output dir'] = self.output_dir
    #     self.relevant_parameters['Genome'] = self.genome
    #     self.relevant_parameters['Gtf file'] = self.gtf
    #     self.relevant_parameters['Adapters'] = self.adapters
    #     self.relevant_parameters['Stranded protocol'] = self.stranded
    #     self.relevant_parameters['Samples'] = self.samples
    #     self.relevant_parameters['Deseq run'] = self.clean_data_form.deseq_run
    #     if self.parsing_post_obj.factors:
    #         self.relevant_parameters['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
    #     if self.parsing_post_obj.batches:
    #         self.relevant_parameters['Batches'] = self.parsing_post_obj.batches

    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in RnaSeqForm(self.username).fields:
            if 'form_id' in RnaSeqForm(self.username).fields[key].widget.attrs and \
                    RnaSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced' :
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in RnaSeqForm(self.username).fields[key].widget.attrs and \
                    RnaSeqForm(self.username).fields[key].widget.attrs['form_id'] == 'basic' and key != "genome" and key != "annotation":
                self.basic[key] = self.post_params[key]
        self.basic["genome"] = self.genome
        self.basic["annotation"] = self.gtf
        self.basic['Samples'] = self.samples
        if self.parsing_post_obj.factors:
            self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
        if self.parsing_post_obj.batches:
            self.basic['Batches'] = self.parsing_post_obj.batches
        self.relevant_parameters['advanced_parameters'] = self.advanced
        self.relevant_parameters['basic_parameters'] = self.basic


    def create_config_file(self):
        with open(self.config_file, 'w') as conf_f:
            conf_f.writelines('---' + '\n')
            conf_f.writelines('run_id: "' + self.date + '"\n')
            conf_f.writelines('job_name: "' + self.job_name + '"\n')
            conf_f.writelines('fastq_dir: ' + self.input_dir + '\n')
            conf_f.writelines('gtf: ' + self.gtf + '\n')
            conf_f.writelines('my_star_index: ' + self.genome + '\n')
            conf_f.writelines('output_dir: ' + self.output_dir + '\n')
            conf_f.writelines('adaptor: ' + self.adapters + '\n')
            conf_f.writelines('stranded_protocol: ' + self.stranded + '\n')
            conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
            conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
            conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
            conf_f.writelines('conda_root: ' + CONDA_ROOT + '\n')
            conf_f.writelines('gs_exe: ' + GS_EXE + '\n')
            conf_f.writelines('Rscript: ' + RSCRIPT + '\n')
            conf_f.writelines('R_lib_paths: ' + R_LIB_PATHS + '\n')
            conf_f.writelines('cutadapt_exe: ' + CUTADAPT_EXE + '\n')
            conf_f.writelines('fastqc_exe: ' + FASTQC_EXE + '\n')
            conf_f.writelines('star_exe: ' + STAR_EXE + '\n')
            conf_f.writelines('samtools_exe: ' + SAMTOOLS_EXE + '\n')
            conf_f.writelines('ngs_plot_exe: ' + NGS_PLOT_EXE + '\n')
            conf_f.writelines('htseq_count_exe: ' + HTSEQ_COUNT_EXE + '\n')
            conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')
            conf_f.writelines('correct_with_FDR_tool: "' + str(self.correct_with_FDR_tool)+ '"\n')
            conf_f.writelines('adjusted_P_value: "' + str(self.adjusted_P_value) + '"\n')
            conf_f.writelines('baseMean: "' + str(self.baseMean) + '"\n')
            conf_f.writelines('log2_fold_change: "' + str(self.log2FC) + '"\n')
            conf_f.writelines('additional_Adapter_R1: ' + self.additional_Adapter_R1 + '\n')
            conf_f.writelines('additional_Adapter_R2: ' + self.additional_Adapter_R2 + '\n')
            conf_f.writelines('cutadapt_qvalue: "' + str(self.cutadapt_qvalue) + '"\n')
            conf_f.writelines('cutadapt_min_len: "' + str(self.cutadapt_min_len) + '"\n')
            conf_f.writelines('cutadapt_cut_start: "' + str(self.cutadapt_cut_start) + '"\n')
            conf_f.writelines('cutadapt_cut_end: "' + str(self.cutadapt_cut_end) + '"\n')
            if self.parsing_post_obj.factors:
                conf_f.writelines('factors_file: ' + self.factors_file + '\n')
                self.parsing_post_obj.create_factors_file()

    def specific_steps_to_run(self):
        return ''


class SubmitRnaSeq(RunRnaSeq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(SubmitRnaSeq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                           params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.results_link_ = "%s/no_results/" % (PIPELINE_URL)

    # Overide the function of RunAnalysis class
    def send_analysis(self):
        try:
            self.save_relevant_parameters()
            self.create_yaml_parameters_file(self.params_yaml_file)
            json = self.create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params,
                                                            self.user, self.output_dir)
            logger.info("Parameters of the analysis are: " + json)
            return None, self.results_link_, self.output_dir
        except Exception as e:
            logger.error('Cannot submit analysis because Error: %s' % str(e))
            return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir


class RunRnaSeqDeseq(RunRnaSeq):
    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunRnaSeqDeseq, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                             params_yaml_file, params_json_file, post_params, date, output_dir,
                                             **kwargs)

    def specific_steps_to_run(self):
        return 'rule_4_reports'
