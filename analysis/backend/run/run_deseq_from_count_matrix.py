"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import logging
import subprocess
import itertools

from analysis.backend.run.run_base import RunAnalysis
from analysis.backend.forms.forms_deseq_to_count_matrix import DESeqFromCountMatrixForm
from analysis.backend.run.parsing_post import ParsingPost
from analysis.backend.run.run_ngs import RunAnalysisNGS
from analysis.backend.utils.users import get_user_dir
from analysis.backend.views.views_general import remote_samples_list_ajax
from abc import ABCMeta, abstractmethod
from analysis.backend.utils.general import  get_samples_names
from analysis.backend.settings import SNAKEFILE_DESEQ_FROM_COUNTS_MATRIX, PIPELINE_URL_HTTP, SNAKEFILE_SCRIPTS, SNAKEFILE_TAMPLATES, SNAKEFILE_PYTHON, RSCRIPT, R_LIB_PATHS, CONDA_ROOT, GS_EXE, MAX_CORES, RESULTS_DIR
from analysis.backend.settings import JOBS_STATUS_FILE, JOB_REPEATS_NUMBER, CLUSTER_EXE, SNAKEMAKE_EXE, PIPELINE_SERVER, \
    USER_CLUSTER, CLUSTER_TYPE, DEMULTIPLEXING_MEMORY, SNAKEFILE_VERSION, UTAP_VERSION, CLUSTER_RESOURCES_PARAMS, RUN_LOCAL, RUN_LOCAL_HOST_CONDA,MAX_CORES



# do we need GS_EXE and MAX_CORES
logger = logging.getLogger(__name__)



class RunDESeqFromCountMatrix(RunAnalysis):
    __metaclass__ = ABCMeta
    pipeline_name_ = 'DESeq from counts matrix'

    def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
                 params_json_file, post_params, date, output_dir, **kwargs):
        super(RunDESeqFromCountMatrix, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
                                         params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
        self.results_link_ = "%s/DESeq_from_counts_matrix/" % (PIPELINE_URL_HTTP)
        if not self.output_dir:  # In case of Submitted analyses the class get output_dir as input
            self.output_dir = os.path.join(self.prj_dir, self.job_name +'_DESeq_from_counts_matrix')
        self.username = username
        # self.input_dir = os.path.abspath(os.path.join(get_user_dir(username), self.clean_data_form.input_folder))
        self.samples = self.get_samples_list(self.clean_data_form.input_folder)[0]
        self.stdout_file = os.path.join(self.output_dir, self.job_name + "_log")
        self.cmd_file = os.path.join(self.output_dir, self.job_name + '_cmd')
        self.results_dir = os.path.join(self.output_dir, "results_" + self.job_name)
        self.report_count_matrix = os.path.join(self.results_dir, "report_count_matrix.Rmd")
        self.report_functions = os.path.join(self.results_dir, "report_functions.R")
        self.desc_out_file = os.path.join(self.output_dir, 'sample_desc_' + self.job_name + '.csv')
        self.comp_out_file = os.path.join(self.output_dir, 'comparisons_' + self.job_name + '.csv')
        self.input_dir = self.clean_data_form.input_folder


        self.parsing_post_obj = ParsingPost(self.samples, self.desc_out_file, self.post_params_obj, self.pipeline) # in this pipeline we dont generate phanodata file
        self.unique_factors= list(sorted(set(self.parsing_post_obj.factors.values()), key=self.parsing_post_obj.factors.values().index)) #get all unique categories from boxes


# Overiding the method get_samples_list in run_ngs
    def get_samples_list(self, path): #calling the function in general
            return  get_samples_names(path)

    def get_prj_dir(self):
        return os.path.abspath(os.path.join(self.user_base_dir, self.clean_data_form.output_folder))
    # def copy_snakefile(self, snakefile_source):
    #     os.system('cp %s %s' % (snakefile_source, self.snakefile_name))
    #     os.system('cp %s %s' % (SNAKEFILE_VERSION, self.snakefile_version))
    #     os.system("sed -i '1iSnakemake version: ' %s" % (self.snakefile_version))
    #     os.system("sed -i '1iUTAP version: %s' %s" % (UTAP_VERSION, self.snakefile_version))

    def copy_rscripts(self):

        os.system('cp %s %s' % (os.path.join(SNAKEFILE_TAMPLATES, "report_functions.R"),self.results_dir))
        os.system('cp %s %s' % (os.path.join(SNAKEFILE_TAMPLATES, "report_count_matrix.Rmd"),self.results_dir))
        os.system('cp %s %s' % (os.path.join(SNAKEFILE_TAMPLATES, "header.html"),self.results_dir))
        os.system('cp %s %s' % (os.path.join(SNAKEFILE_TAMPLATES, "wis_logo_heb_v1.png"),self.results_dir))
        os.system('cp -R %s %s' % (os.path.join(SNAKEFILE_TAMPLATES, "templates"),self.results_dir))


    def valid_factors(self):
        deseq_samples =[]
        factors =[]
        if not self.parsing_post_obj.factors:
            logging.info('no factors - DESeq was not run')
            return False
        else:
            logger.info(self.parsing_post_obj.factors.items())
            for sample_name, category_name in self.parsing_post_obj.factors.items():
                deseq_samples.append(sample_name)
                factors.append(category_name)
            if len(deseq_samples) != len(factors):
                 logging.error('The numbers of the samples and their factors is not equal.')
                 return False
            if len(self.unique_factors) == 1:
                 logging.error('There is only one factor.')
                 return False
            if self.parsing_post_obj.batches:
                    if len(list(set(self.parsing_post_obj.batches))) == 1:
                        logging.error('There is only one batch')
                        return False
                    if len(self.parsing_post_obj.batches) != len(self.parsing_post_obj.factors):
                        logging.error('The numbers of the factors and their batches is not equal.')
                        return False
                    else:
                       logging.info('There is %d samples and %d descriptions' % (len(deseq_samples), len(factors)))
                       return True
            else:  # There are factors but not batch
                    logging.info('There is %d samples and %d descriptions' % (len(deseq_samples), len(factors)))
                    return True



    def create_desc_file(self):
        desc_file = open(self.desc_out_file, 'w')
        if self.parsing_post_obj.batches:
            desc_file.writelines('\t'.join(['Sample', 'condition', 'batches']) + '\n')
            for sample_name, category_name in self.parsing_post_obj.factors.items():
                batch = self.parsing_post_obj.batches[sample_name] if sample_name in self.parsing_post_obj.batches else 'Batch-' + str(MAX_BATCHES + 1)
                desc_file.writelines('\t'.join([sample_name, category_name, batch]) + '\n')
        else:
            desc_file.writelines('\t'.join(['Sample', 'condition']) + '\n')
            for sample_name, category_name in self.parsing_post_obj.factors.items():
                desc_file.writelines('\t'.join([sample_name, category_name]) + '\n')
        desc_file.close()


    def create_comp_file(self):
        comp_file = open(self.comp_out_file, 'w')
        comp_file.writelines('\t'.join(['Comparison', 'Factor', 'A', 'B', 'Formula']) + '\n')
        for pair in itertools.combinations(self.unique_factors, r=2):
            vs = pair[0] + '_vs_' + pair[1]
            Or = pair[0] + '_or_' + pair[1]
            comp_file.writelines('\t'.join([vs, Or, pair[0], pair[1], Or]) + '\n')
        comp_file.close()

    def print_empty_files(self):
        with open(self.desc_out_file, 'w') as descfile:
            descfile.writelines([])
        with open(self.comp_out_file, 'w') as compfile:
            compfile.writelines([])


    def create_comparisons_files(self):
        if self.valid_factors():
            self.create_desc_file()
            self.create_comp_file()
        else:
            self.print_empty_files()




    def create_constants(self):
        os.system("sed -i \'s/^CORRECT_BY_FDRTOOL=.*/%s/g\' %s" % (
            "correct_by_fdrtool = TRUE" if self.clean_data_form.correct_with_FDR_tool == 'yes' else "correct_by_fdrtool = FALSE",self.report_count_matrix))
        os.system("sed -i \'s/PADJ=[^,\)]*/%s/g\' %s" % (
            "padj = " + str(self.clean_data_form.adjusted_P_value), self.report_count_matrix))
        os.system("sed -i \'s/BASE_MEAN=[^,\)]*/%s/g\' %s" % (
            "baseMean = "+ str(self.clean_data_form.baseMean), self.report_count_matrix))
        os.system("sed -i \'s/LOG2FOLD_CHANGE=[^,\)]*/%s/g\' %s" % (
            "log2FoldChange = " + str(self.clean_data_form.log2_Fold_Change), self.report_count_matrix))
        os.system("sed -i \'s@JOB_NAME@%s@g\' %s" % (self.job_name, self.report_count_matrix))
        os.system("sed -i \'s@RUN_ID@%s@g\' %s" % (self.date, self.report_count_matrix))
        os.system("sed -i \'s@RSCRIPT@%s@g\' %s" % ("\""+RSCRIPT+"\"", self.report_count_matrix))
        os.system("sed -i \'s@R_LIB_PATHS@%s@g\' %s" % ("\""+R_LIB_PATHS+"\"", self.report_functions))
        os.system("sed -i \'s@PIPELINE_TYPE@%s@g\' %s" % (self.pipeline, self.report_count_matrix))
        os.system("sed -i \'s@INTERMINE_WEB_QUERY@%s@g\' %s" % (
        self.clean_data_form.intermine_genome.interMine_web_query, self.report_count_matrix))
        os.system("sed -i \'s@INTERMINE_WEB_BASE@%s@g\' %s" % (
        self.clean_data_form.intermine_genome.intermine_web_base, self.report_count_matrix))
        os.system("sed -i \'s@INTERMINE_CREATURE@%s@g\' %s" % (
        self.clean_data_form.intermine_genome.interMine_creature, self.report_count_matrix))
        os.system("sed -i \'s@INTERMINE_CREATURE@%s@g\' %s" % (
        self.clean_data_form.intermine_genome.interMine_creature, self.report_count_matrix))
        os.system("sed -i \'s@INPUT_FOLDER@%s@g\' %s" % (self.input_dir[1:], self.report_count_matrix))
        os.system("sed -i \'s@OUTPUT_FOLDER@%s@g\' %s" % (self.results_dir[1:], self.report_count_matrix))
        os.system("sed -i \'s@../COUNTS_MATRIX_FILE@%s@g\' %s" % (self.input_dir, self.report_count_matrix))
        os.system("sed -i \'s@REPORT_OUTPUT_DIR@%s@g\' %s" % (self.results_dir[1:], self.report_count_matrix))
        os.system("sed -i \'s@SUBTITLE@@g\' %s" % self.report_count_matrix)
        os.system("sed -i \'s@COMPARISONS_CSV@%s@g\' %s" % (os.path.basename(self.comp_out_file),self.report_count_matrix))
        os.system("sed -i \'s@SAMPLE_DESC_CSV@%s@g\' %s" % (os.path.basename(self.desc_out_file),self.report_count_matrix))
        os.system("sed -i \'s@GENE_DB_URL@%s@g\' %s" % (
            "\"" +self.clean_data_form.intermine_genome.gene_db_url +"\"", self.report_count_matrix))
    # sed - i \'s/GENE_DB_URL/\"{GENE_DB_URL}\"/g\' {params.out_dir_report}/report.Rmd






    def prepare_cmd_file(self, allowed_step=None):
        cluster_err = 'cluster_%s.error' % self.date
        cluster_out = 'cluster_%s.output' % self.date

        if not RUN_LOCAL:
            if CLUSTER_TYPE == 'lsf':
                cmd = 'LSB_JOB_REPORT_MAIL=N; cd %s && %s -q %s -o %s -e %s -n 10 -R \"rusage[mem=3000]\" -R \"span[hosts=1]\" %s -e \"rmarkdown::render(\'%s\')\" --verbose &> %s' % (
                    self.output_dir, CLUSTER_EXE, self.queue, cluster_out, cluster_err, os.path.join(RSCRIPT),
                    self.report_count_matrix, self.stdout_file)

            if CLUSTER_TYPE == 'pbs':
                cmd = 'cd %s; %s -m n -q %s -o %s -e %s -n 10 -R \"rusage[mem=3000]\" -R \"span[hosts=1]\" %s -e \"rmarkdown::render(\'%s\')\" --verbose &> %s' % (
                    self.results_dir, CLUSTER_EXE, self.queue, cluster_out, cluster_err,
                    self.stdout_file)
        else:  # run on local machine
           cmd= 'cd %s &> %s' % (
                self.output_dir,self.stdout_file)
        with open(self.cmd_file, 'w') as sf:
            sf.writelines("#!/bin/bash\n")
            sf.writelines("set history = 1\n")  # For tcsh shell
            sf.writelines("export HISTSIZE=1\n")  # For bash shell
            sf.writelines("export HOME=$HOME\n")  # For bash shell
            sf.writelines("PATH=$PATH:%s\n" % (os.path.join(SNAKEFILE_SCRIPTS)))
            sf.writelines(cmd + '\n')
            sf.writelines('rc=$?\n')
            sf.writelines('echo "Command return code for job %s $rc" >> %s\n' % (self.job_name, JOBS_STATUS_FILE))
            sf.writelines('cat %s_* >> %s\n' % (cluster_err, cluster_err))
            sf.writelines('cat %s_* >> %s\n' % (cluster_out, cluster_out))
            sf.writelines('rm -f %s_* %s_*\n' % (cluster_out, cluster_err))
            sf.writelines('cp %s commands_log_%s.txt\n' % (cluster_err, self.date))
            sf.writelines('mv commands_log_%s.txt *_reports\n' % self.date)
            sf.writelines('date\n')
            sf.writelines('chmod -R ug+w %s\n' % self.output_dir)
        os.system('chmod ug+x %s \n' % self.cmd_file)


# files to change: SAMPLE_DESC_CSV, CORRECT_BY_FDRTOOL, LOG2FOLD_CHANGE, BASE_MEAN, RUN_ID, COUNTS_MATRIX_FILE, COMPARISONS_CSV, RSCRIPT, INPUT_FOLDER, OUTPUT_FOLDER,INTERMINE_WEB_QUERY
    # INTERMINE_WEB_BASE, INTERMINE_CREATURE, R_LIB_PATHS
    def run_cmd(self):
        # close_fds: release the port, the website of the client can to continue run
        subprocess.Popen(os.path.join(self.output_dir,self.cmd_file), shell=True, close_fds=True)

    def run_cmd_ssh(self):
        # close_fds: release the port, the website of the client can to continue run
        # Example: ssh -l USERNAME SERVERNAME "qsub -o out -e err -q S com.sh"
        ssh_command = 'ssh  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l %s %s "bash -s; export HISTSIZE=1; set history = 1" < %s' % (
            USER_CLUSTER, PIPELINE_SERVER, self.cmd_file)
        logger.info(ssh_command)
        subprocess.Popen(ssh_command, shell=True, close_fds=True)

    def specific_steps_to_run(self):
        return 'rule_10_reports'

    # def run_snakefile_ssh(self):
    #     # close_fds: release the port, the website of the client can to continue run
    #     # Example: ssh -l USERNAME SERVERNAME "qsub -o out -e err -q S com.sh"
    #     ssh_command = 'ssh  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l %s %s "bash -s; export HISTSIZE=1; set history = 1" < %s' % (
    #         USER_CLUSTER, PIPELINE_SERVER, self.cmd_file)
    #     logger.info(ssh_command)
    #     subprocess.Popen(ssh_command, shell=True, close_fds=True)

    # Overiding the method run_commands in run_ngs
    def run_commands(self):
        os.system('mkdir %s; cd %s' % (self.output_dir,self.output_dir))
        self.create_comparisons_files()
        self.prepare_cmd_file(allowed_step=self.specific_steps_to_run())
        os.system('mkdir -p %s; cd %s' % (self.results_dir, self.results_dir))

        self.copy_rscripts()
        self.create_constants()
        # os.system('ln -s %s %s' % (self.results_dir, os.path.join(RESULTS_DIR, self.job_name)))
        if RUN_LOCAL and not RUN_LOCAL_HOST_CONDA:
            self.run_cmd()
        else:
            self.run_cmd_ssh()




    def save_relevant_parameters(self):
        self.basic = {}
        self.advanced = {}
        for key in  DESeqFromCountMatrixForm(self.username).fields:
            if 'form_id' in DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs and \
                    DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
                self.advanced[key] = self.post_params[key]
            elif 'form_id' in DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs and \
                    DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs['form_id'] == 'basic':
                self.basic[key] = self.post_params[key]
        self.basic['Samples'] = self.samples
        if self.parsing_post_obj.factors:
             self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
        if self.parsing_post_obj.batches:
             self.basic['Batches'] = self.parsing_post_obj.batches
        self.relevant_parameters['advanced_parameters'] = self.advanced
        self.relevant_parameters['basic_parameters'] = self.basic
#if running sankemake
# class SubmitDESeqFromCountMatrix(RunDESeqFromCountMatrix):
#     def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
#                  params_json_file, post_params, date, output_dir, **kwargs):
#         super(RunDESeqFromCountMatrix, self).__init__(username, run_by_user, job_name, pipeline, request, form,
#                                                       queue,
#                                                       params_yaml_file, params_json_file, post_params, date,
#                                                       output_dir,
#                                                       **kwargs)
#         self.results_link_ = "%s/no_results/" % (PIPELINE_URL_HTTP)
#
    # def send_analysis(self):
    #     try:
    #         self.send()
    #         self.save_relevant_parameters()
    #         self.create_yaml_parameters_file(self.params_yaml_file)
    #         json = create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params, self.user,
    #                                                    self.output_dir)
    #         logger.info("Parameters of the analysis are: " + json)
    #         return None, self.results_link_, self.output_dir
    #     except Exception as e:
    #         logger.error('Cannot submit analysis because Error: %s' % str(e))
    #         return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir



    # Overide the function of RunAnalysis class
    def send(self):
        self.run_commands()

#
# class DESeqRunFromCountMatrix(RunDESeqFromCountMatrix):
#     def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
#                  params_json_file, post_params, date, output_dir, **kwargs):
#         super(DESeqRunFromCountMatrix, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
#                                               params_yaml_file, params_json_file, post_params, date, output_dir,
#                                               **kwargs)


# class RunDESeqFromCountMatrix(RunAnalysisNGS):
#     __metaclass__ = ABCMeta
#     pipeline_name_ = 'DESeq from counts matrix'
#
#     def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
#                  params_json_file, post_params, date, output_dir, **kwargs):
#         super(RunDESeqFromCountMatrix, self).__init__(username, run_by_user, job_name, pipeline, request, form, queue,
#                                          params_yaml_file, params_json_file, post_params, date, output_dir, **kwargs)
#         self.results_link_ = "%s/DESeq_from_counts_matrix/" % (PIPELINE_URL_HTTP)
#         if not self.output_dir:  # In case of Submitted analyses the class get output_dir as input
#             self.output_dir = os.path.join(self.prj_dir, self.job_name +'_DESeq_from_counts_matrix')
#         self.username = username
#         # self.input_dir = os.path.abspath(os.path.join(get_user_dir(username), self.clean_data_form.input_folder))
#         self.samples = self.get_samples_list(self.clean_data_form.input_folder)[0]
#         self.input_dir = self.clean_data_form.input_folder
#         self.results_dir = os.path.join(self.output_dir, "results_" + self.job_name)
#         self.snakefile_source = SNAKEFILE_DESEQ_FROM_COUNTS_MATRIX
#         self.config_file = os.path.join(self.output_dir, 'config-deseq-from-counts-matrix-%s.yaml' % self.date)
#
#         self.factors_file = None if self.clean_data_form.deseq_run == 'no' else os.path.join(
#             self.output_dir, 'pheno_data-%s.tsv' % self.date)
#         self.parsing_post_obj = ParsingPost(self.samples, self.factors_file, self.post_params_obj, self.pipeline) # in this pipeline we dont generate phanodata file
#         self.unique_factors= list(sorted(set(self.parsing_post_obj.factors.values()), key=self.parsing_post_obj.factors.values().index)) #get all unique categories from boxes
#         self.factors= ['no-deseq'] if not self.parsing_post_obj.factors else []
#         self.deseq_samples=[]
#         for sample_name, category_name in self.parsing_post_obj.factors.items():
#             self.deseq_samples.append(sample_name)
#             self.factors.append(category_name)
#         self.batches= self.parsing_post_obj.batches.values()
#
# # Overiding the method get_samples_list in run_ngs
#     def get_samples_list(self, path): #calling the function in general
#             return  get_samples_names(path)
#
#
#     def create_config_file(self):
#         with open(self.config_file, 'w') as conf_f:
#             conf_f.writelines('run_id: "' + self.date + '"\n')
#             conf_f.writelines('---\n')
#             conf_f.writelines('job_name: "' + self.job_name + '"\n')
#             conf_f.writelines('input_file: ' + self.input_dir + '\n')
#             conf_f.writelines('output_dir: ' + self.output_dir + '\n')
#             conf_f.writelines('scripts: ' + SNAKEFILE_SCRIPTS + '\n')
#             conf_f.writelines('templates: ' + SNAKEFILE_TAMPLATES + '\n')
#             conf_f.writelines('python: ' + SNAKEFILE_PYTHON + '\n')
#             conf_f.writelines('Rscript: ' + RSCRIPT + '\n')
#             conf_f.writelines('R_lib_paths: ' + R_LIB_PATHS + '\n')
#             conf_f.writelines('conda_root: ' + CONDA_ROOT + '\n')
#             conf_f.writelines('factors: ' + self.factors + '\n')
#             conf_f.writelines('deseq_samples: ' + self.deseq_samples + '\n')
#             conf_f.writelines('max_threads_num: ' + MAX_CORES + '\n')
#             conf_f.writelines('correct_with_FDR_tool: "' + str(self.correct_with_FDR_tool) + '"\n')
#             conf_f.writelines('adjusted_P_value: "' + str(self.adjusted_P_value) + '"\n')
#             conf_f.writelines('baseMean: "' + str(self.baseMean) + '"\n')
#             conf_f.writelines('log2_fold_change: "' + str(self.log2FC) + '"\n')
#             conf_f.writelines('gene_db: "' + self.clean_data_form.intermine_genome.gene_db_url + '"\n')
#             conf_f.writelines('intermine_web_query: "' + self.clean_data_form.intermine_genome.interMine_web_query + '"\n')
#             conf_f.writelines('intermine_web_base: "' + self.clean_data_form.intermine_genome.intermine_web_base + '"\n')
#             conf_f.writelines('intermine_web_creature: "' + intermine_genome.interMine_creature + '"\n')
#             if self.parsing_post_obj.factors:
#                 conf_f.writelines('factors_file: ' + self.factors_file + '\n')
#                 self.parsing_post_obj.create_factors_file()
#
#
#
#     def save_relevant_parameters(self):
#         self.basic = {}
#         self.advanced = {}
#         for key in  DESeqFromCountMatrixForm(self.username).fields:
#             if 'form_id' in DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs and \
#                     DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs['form_id'] == 'advanced':
#                 self.advanced[key] = self.post_params[key]
#             elif 'form_id' in DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs and \
#                     DESeqFromCountMatrixForm(self.username).fields[key].widget.attrs['form_id'] == 'basic':
#                 self.basic[key] = self.post_params[key]
#         self.basic['Samples'] = self.samples
#         if self.parsing_post_obj.factors:
#              self.basic['Categories'] = dict(self.parsing_post_obj.factors)  # yaml cannot handle OrderedDict object
#         if self.parsing_post_obj.batches:
#              self.basic['Batches'] = self.parsing_post_obj.batches
#         self.relevant_parameters['advanced_parameters'] = self.advanced
#         self.relevant_parameters['basic_parameters'] = self.basic
#
#
#     def specific_steps_to_run(self):
#         return ''
#
#
# class SubmitDESeqFromCountMatrix(RunDESeqFromCountMatrix):
#     def __init__(self, username, run_by_user, job_name, pipeline, request, form, queue, params_yaml_file,
#                  params_json_file, post_params, date, output_dir, **kwargs):
#         super(RunDESeqFromCountMatrix, self).__init__(username, run_by_user, job_name, pipeline, request, form,
#                                                       queue,
#                                                       params_yaml_file, params_json_file, post_params, date,
#                                                       output_dir,
#                                                       **kwargs)
#         self.results_link_ = "%s/no_results/" % (PIPELINE_URL_HTTP)
#
#
#
#     # Overide the function of RunAnalysis class
#     def send_analysis(self):
#         try:
#             self.save_relevant_parameters()
#             self.create_yaml_parameters_file(self.params_yaml_file)
#             json = self.create_json_parameters_file_to_post(self.params_json_file, self.form, self.post_params,
#                                                             self.user,
#                                                             self.output_dir)
#             logger.info("Parameters of the analysis are: " + json)
#             return None, self.results_link_, self.output_dir
#         except Exception as e:
#             logger.error('Cannot submit analysis because Error: %s' % str(e))
#             return Exception('Cannot submit analysis because Error: %s' % str(e)), self.results_link_, self.output_dir
#

