# change name of this file to settings.py

from settings_base import *

BBCU_INTERNAL = False

COLLABORATIONS_DIR = ''

#Not relevant to external ----
QUEUE_STAFF = ''
NEXTSEQ_SERVER = ''
NEXTSEQ_USER = ''
FASTQ_REMOTE_PATH = ''
FASTQ_REMOTE_PATH_INCPM = ''
BCL_REMOTE_PATH = ''
BCL_REMOTE_PATH_INCPM = ''
BCL_REMOTE_PATH_INCPM_NOVA = ''

#-------

DOCKER_USERS = 'django-users' #group of users on docker (for each signed up user created user on the docker with his permissions in order to deny other user to access to its files)
