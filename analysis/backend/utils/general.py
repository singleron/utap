"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os
import subprocess
from collections import namedtuple
from glob import glob

import paramiko

from analysis.backend.settings import LAB_DIR, MIN_DISK_FREE, PIPELINE_OUTPUT_FOLDER, BBCU_INTERNAL, COLLABORATIONS_DIR, \
    QUEUE_STAFF, QUEUE_USERS, USER_CLUSTER, PIPELINE_SERVER
from analysis.backend.utils.consts import SINGLECELL_CELRANGER_ANALYSIS_TYPE, MARSSEQ_ANALYSIS_TYPE

logger = logging.getLogger(__name__)

DiskUsage = namedtuple('DiskUsage', 'total used free')


def get_cluster_queue(request):
    if not BBCU_INTERNAL:
        return QUEUE_USERS
    elif request.user.is_superuser:
        if request.user.username == 'submit':
            return QUEUE_USERS
        else:
            return QUEUE_STAFF
    else:
        return QUEUE_USERS


def disk_usage(path):
    """Return disk usage statistics about the given path.

    Will return the namedtuple with attributes: 'total', 'used' and 'free',
    which are the amount of total, used and free space, in bytes.
    """
    st = os.statvfs(path)
    free = int((st.f_bavail * st.f_frsize) / (1024 ** 3))
    total = int((st.f_blocks * st.f_frsize) / (1024 ** 3))
    used = int((st.f_blocks - st.f_bfree) * st.f_frsize) / (1024 ** 3)
    return DiskUsage(total, used, free)


def open_remote_ssh_session(server_name, server_user, server_pass):
    try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server_name, 22, username=server_user, password=server_pass)
        return ssh
    except Exception as e:
        raise Exception('Cannot connect to Stefan with ssh: %s' % str(e))


def folder_exists(dir):
    if ' ' in dir:
        return False, 'The path: %s \ncannot contain spaces characters, please change the name of the directories.' % dir
    if not os.path.isdir(dir):
        return False, 'No such folder: ' + dir + ', please select another existing folder'
    else:
        return True, True


# free space in Gb
def has_space_on_disc(dir):
    free = disk_usage(dir).free
    if free < MIN_DISK_FREE:
        return False, 'The free disk space of your lab is %sG. The analysis may require until %sG of free space. Please remove old file before running the analysis.' % (
            free, MIN_DISK_FREE)
    else:
        return True, 'The free disk space of your lab is %sG.' % free


def root_dir_of_cellranger(root_dir):
    out_dir = os.path.join(root_dir, 'outs')
    if not os.path.isdir(out_dir):
        return False, 'No \"outs\" directory inside %s directory' % root_dir
    fastq_path = os.path.join(out_dir, 'fastq_path')
    if not os.path.isdir(fastq_path):
        return False, 'No \"fastq_path\" directory inside %s directory' % root_dir
    return True, fastq_path


def samples_in_fastq_dir(root_dir, pipeline):
    if pipeline == SINGLECELL_CELRANGER_ANALYSIS_TYPE:
        logger.info('User want to get samples of cellranger in directory %s' % root_dir)
        status, path = root_dir_of_cellranger(root_dir)
        if not status:
            return status, path
        root_dir = path

    logger.info('User want to get samples of %s in directory %s' % (pipeline, root_dir))

    num_samples = 0
    samples = []
    if ' ' in root_dir:
        return False, 'The path: %s \ncannot contain spaces characters, please change the name of the directories.' % root_dir
    if os.path.isdir(root_dir):
        paired_end = None
        for sample_dir in os.listdir(root_dir):
            if sample_dir.startswith('.'):
                continue
            full_sample_dir = os.path.join(root_dir, sample_dir)
            if os.path.isdir(full_sample_dir):
                s = sample_dir
                if sample_dir == 'Undetermined':
                    continue
                # if pipeline == SINGLECELL_CELRANGER_ANALYSIS_TYPE or sample_dir == 'Stats' or sample_dir == 'Reports':
                if sample_dir == 'FastQC' or sample_dir == 'FastQCinput' or sample_dir == 'Stats' or sample_dir == 'Reports' or sample_dir == 'SampleSheet.csv' or sample_dir == 'Mars-seq_users.xlsx':
                    continue
                if ' ' in sample_dir:
                    return False, 'The sample name of sample folder %s \ncannot contain spaces characters, please change the name of the directories.' % sample_dir
                listdir = os.listdir(full_sample_dir)
                included_extensions = ['fq', 'fastq', 'fq.gz', 'fastq.gz']
                not_fastq_files = [f for f in listdir
                                   if not f.startswith('.') and not any(f.endswith(ext) for ext in included_extensions)]
                if not_fastq_files:
                    return False, 'The sample %s \ncontains non-fastq files (you need to delete them).' % sample_dir

                glob_r1 = glob(os.path.join(full_sample_dir, '*_R1*.fastq')) + glob(
                    os.path.join(full_sample_dir, '*_R1*.fastq.gz')) + glob(
                    os.path.join(full_sample_dir, '*_R1_0*.fastq')) + glob(
                    os.path.join(full_sample_dir, '*_R1_0*.fastq.gz'))
                glob_r1 = list(set(glob_r1))
                glob_r2 = glob(os.path.join(full_sample_dir, '*_R2*.fastq')) + glob(
                    os.path.join(full_sample_dir, '*_R2*.fastq.gz')) + glob(
                    os.path.join(full_sample_dir, '*_R2_0*.fastq')) + glob(
                    os.path.join(full_sample_dir, '*_R2_0*.fastq.gz'))
                glob_r2 = list(set(glob_r2))

                if paired_end is None:
                    paired_end = True if glob_r2 else False
                if pipeline == MARSSEQ_ANALYSIS_TYPE and not paired_end:
                    return False, 'MARS-Seq pipeline requires paired-end data (*R1* and *R2* files in each sample), But sample %s contains only *R1* file.' %s
                if (glob_r2 and not paired_end) or (not glob_r2 and paired_end):
                    return False, 'Part of the samples are paired-end and other are single-read'
                if not glob_r1:
                    return False, 'No valid *R1* fastq files in sample folder %s. \nThe fastq files must be in this format: %s_R[1|2]*.fq(.gz) \nor %s_R[1|2]*.fastq(.gz) \nor %s_R2.fq \nor %s_R2.fastq(.gz) ' % (
                        s, s, s, s, s)
                if len(glob_r1) > 1 and not True in [True if '_R1_0' in f else False for f in glob_r1]:
                    return False, 'There is more than two *R1* files in sample folder %s. In each sample folder can be only one *R1* file, or more than one file if the file names are in this format *_R1_001*.fq, *_R1_002*.fq, etc ...' % sample_dir
                if len(glob_r2) > 1 and not True in [True if '_R2_0' in f else False for f in glob_r2]:
                    return False, 'There is more than two *R2* files in sample folder %s. In each sample folder can be only one *R2* file, or more than one file if the file names are in this format *_R2_001*.fq, *_R2_002*.fq, etc ...' % sample_dir
                if glob_r2 and (len(glob_r2) > 1 or len(glob_r1) > 1):
                    if len(glob_r1) != len(glob_r2):
                        return False, 'When the file names are in format *_R[1|2]_0* - the file numbers of *R1* and *R2* files must be equeal in folder %s' % sample_dir
                    r2_replaced = [f.replace('R2', 'R1') for f in glob_r2]
                    for r1, r2 in zip(sorted(glob_r1), sorted(r2_replaced)):
                        if r1 != r2:
                            return False, 'When the file names are in format *_R[1|2]_0* - the file names of *R2* files must be equal to *R1* file names in folder %s' % sample_dir
                num_samples += 1
                samples.append(sample_dir)

        if num_samples < 1:  # at least 2 valid samples
            return False, 'No valid sample folders in %s' % root_dir

        logger.info('User selected these samples %s' % ' '.join(samples))
        return sorted(samples), num_samples
    return False, 'There is no such directory: %s' % root_dir

def get_samples_names(root_dir):
    if os.path.isfile(root_dir):
        if ' ' in root_dir:
            return False, 'The input file: %s \ncannot contain spaces characters, please change the name of the input file.' % root_dir
        file_extension = os.path.splitext(root_dir)[1]
        with open(root_dir, 'r') as input_file:
            if file_extension == '.csv':
                samples = input_file.readlines()[0].split(',')
                num_samples = len(samples)
            elif file_extension == '.txt':
                samples = input_file.readlines()[0].split()
                num_samples  = len(samples)
            else:
                return False,'' #'Please select csv or txt file format as input file. \n the file %s that you have selected is not in the right format' %root_dir
        samples = list(set(samples))
        logger.info(samples)
        logger.info('User selected these samples %s' % ' '.join(samples))
        return sorted(samples), num_samples
    return False, 'There is no such file: %s' % root_dir




def get_user_labname_on_wexac(username):
    process = subprocess.Popen(
        'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l %s %s getent passwd | grep ^%s:' % (
            USER_CLUSTER, PIPELINE_SERVER, username),
        stdout=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate()
    if stderr:
        error_msg = 'the command \"getend passwd\" for user %s is failed: ' % username
        logger.error(error_msg)
        raise Exception(error_msg)
    return os.path.basename(os.path.dirname(stdout.split(':')[5]))


# In each login the fuction is called to check the updated lab of the user
def updated_user_dir(username):
    if not BBCU_INTERNAL:
        return os.path.join(LAB_DIR, username)

    user_dir = None
    try:
        if username == 'admin':
            user_dir = os.path.join(LAB_DIR, username)
        elif username == 'testuser' or username == 'submit':
            user_dir = os.path.join(LAB_DIR, username)
        elif get_user_labname_on_wexac(username) == "bioservices":
            user_dir = os.path.join(COLLABORATIONS_DIR, get_user_labname_on_wexac(username), 'Collaboration')
            #user_dir = os.path.join(LAB_DIR, username)
        elif 'class' in username:
            user_dir = os.path.join(COLLABORATIONS_DIR, 'testing', 'Collaboration', 'users', username)
        else:
            user_dir = os.path.join(COLLABORATIONS_DIR, get_user_labname_on_wexac(username), 'Collaboration')
        return user_dir
    except Exception as e:
        raise Exception(
            'No %s folder within %s folder in collaboration folder for user %s, the exception is: %s' % (
                PIPELINE_OUTPUT_FOLDER, user_dir, username, str(e)))

