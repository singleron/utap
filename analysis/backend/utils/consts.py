"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.utils.translation import ugettext_lazy as _
from analysis.backend.settings import BBCU_INTERNAL, RUN_LOCAL
from django import forms

ADVANCED_ID = 'advanced'
BASIC_ID = 'basic'

FUTURE_DATA_TYPE = 'future'
DEMULTIPLEXING_ANALYSIS_TYPE = 'Demultiplexing'
DEMULTIPLEXING_BCL_ANALYSIS_TYPE = 'Demultiplexing_from_BCL'
DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE = 'Demultiplexing_from_FASTQ'
DEMULTIPLEXING_RUNID_ANALYSIS_TYPE = 'Demultiplexing_from_RUNID'
RNASEQ_ANALYSIS_TYPE = 'Transcriptome RNA-seq'
MARSSEQ_ANALYSIS_TYPE = 'Transcriptome Mars-seq'
RNASEQ_DESEQ_ANALYSIS_TYPE = 'Transcriptome RNA-seq Deseq'
MARSSEQ_DESEQ_ANALYSIS_TYPE = 'Transcriptome Mars-seq Deseq'
SINGLECELL_CELRANGER_ANALYSIS_TYPE = 'Singlecell Cellranger'
ATACSEQ_ANALYSIS_TYPE = 'ATAC-seq'
CHIPSEQ_ANALYSIS_TYPE = 'CHIP-seq'
DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE = 'DESeq from counts matrix'
CORE_APP = ''  # Field form with this application (and advanced_id) will be written before APP title
BOWTIE = 'bowtie'
BOWTIE2 = 'BOWTIE2'
DESEQ2 = 'DESeq2'
CUTADAPT = 'CUTADAPT'
MACS2 = 'MACS2'
STAR = 'STAR'
Differential_gene_expression_analysis = 'Differential_gene_expression_analysis'
# the DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE is commented since its not working (19.1.20) after fixing the pipeline we have to uncomment it
# ANALYSIS_TYPES_DISPLAY = ((RNASEQ_ANALYSIS_TYPE, _(RNASEQ_ANALYSIS_TYPE)),
#                           (MARSSEQ_ANALYSIS_TYPE, _(MARSSEQ_ANALYSIS_TYPE)),
#                           (DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE, _(DEMULTIPLEXING_FASTQ_ANALYSIS_TYPE)),
#                           (DEMULTIPLEXING_BCL_ANALYSIS_TYPE, _(DEMULTIPLEXING_BCL_ANALYSIS_TYPE)))
ANALYSIS_TYPES_DISPLAY = ((RNASEQ_ANALYSIS_TYPE, _(RNASEQ_ANALYSIS_TYPE)),
                          (MARSSEQ_ANALYSIS_TYPE, _(MARSSEQ_ANALYSIS_TYPE)),
                          (DEMULTIPLEXING_BCL_ANALYSIS_TYPE, _(DEMULTIPLEXING_BCL_ANALYSIS_TYPE)),
                          (DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE, _(DESEQ_FROM_COUNTS_MATRIX_ANALYSIS_TYPE)))

ANALYSIS_TYPES_HIDE = ((MARSSEQ_DESEQ_ANALYSIS_TYPE, _(MARSSEQ_DESEQ_ANALYSIS_TYPE)),
                       (RNASEQ_DESEQ_ANALYSIS_TYPE, _(RNASEQ_DESEQ_ANALYSIS_TYPE)))
ANALYSIS_TYPES_DISPLAY_SUPERUSER = ANALYSIS_TYPES_DISPLAY # For singlecell pipeline, it should be displayed for BBCU users (superuser) only

# For chromatin pipelines (ATAC-Seq and CHIP-seq) - for now not installed inside the docker:
ANALYSIS_TYPES_NOT_LOCAL = (
    (ATACSEQ_ANALYSIS_TYPE, _(ATACSEQ_ANALYSIS_TYPE)), (CHIPSEQ_ANALYSIS_TYPE, _(CHIPSEQ_ANALYSIS_TYPE)))
if not RUN_LOCAL:
    ANALYSIS_TYPES_DISPLAY += ANALYSIS_TYPES_NOT_LOCAL
    ANALYSIS_TYPES_DISPLAY_SUPERUSER += ANALYSIS_TYPES_NOT_LOCAL        

if BBCU_INTERNAL:
    ANALYSIS_TYPES_DISPLAY_SUPERUSER += ((DEMULTIPLEXING_RUNID_ANALYSIS_TYPE, _(DEMULTIPLEXING_RUNID_ANALYSIS_TYPE)),
                               (SINGLECELL_CELRANGER_ANALYSIS_TYPE, _(SINGLECELL_CELRANGER_ANALYSIS_TYPE)))                         
    ANALYSIS_TYPES_DISPLAY += ((DEMULTIPLEXING_RUNID_ANALYSIS_TYPE, _(DEMULTIPLEXING_RUNID_ANALYSIS_TYPE)),) 
    ANALYSIS_TYPES = ANALYSIS_TYPES_DISPLAY + ANALYSIS_TYPES_HIDE
else:
    ANALYSIS_TYPES = ANALYSIS_TYPES_DISPLAY + ANALYSIS_TYPES_HIDE


# NGS: Create such APP for interior applications within the pipeline
# CONSENSUS_APP = 'Consensus' #Field form with this application (and advanced_id) will be written under this APP title

class RunStatus(object):
    CREATED = 'CREATED'
    SUBMITTED = 'SUBMITTED'
    RUNNING = 'RUNNING'
    SUCCESSFUL = 'SUCCESSFUL'
    FAILED = 'FAILED'
    KILLED = 'KILLED'
    STOPPED = 'STOPPED'
    COPY = 'COPY FASTQ FILES'
    DELETED = 'DELETED'


####################################################################################


class DemultConst(object):
    TRUE_SEQ = 'TruSeq'
    MARS_SEQ = 'MARS-seq'
    RUN_ID_K = 'Run id from Stefan server'
    FASTQ_K = 'Fastq files'
    BCL_K = 'BCL directory'
    RUN_ID_V = 'run_id'
    FASTQ_V = 'fastq'
    BCL_V = 'bcl'

    INPUT_TYPE_TYPES = ((RUN_ID_V, _(RUN_ID_K)), (FASTQ_V, _(FASTQ_K)), (BCL_V, _(BCL_K)))
    INPUT_TYPE_CHOICES = (('', '---------')) + INPUT_TYPE_TYPES
    INPUT_TYPE_HELP = "Type of input files"
    INPUT_TYPE_LABEL = "Input type"

    PROTOCOLS_TYPES = ((TRUE_SEQ, _(TRUE_SEQ)), (MARS_SEQ, _(MARS_SEQ)))
    PROTOCOL_INIT = TRUE_SEQ
    PROTOCOL_HELP = "Sample preparation protocol."
    PROTOCOL_LABEL = "Sample Preparation Protocol"
    PROTOCOL_CHOICES = PROTOCOLS_TYPES


class ATACseqConst(object):
    COMMANDS = [CUTADAPT, MACS2]
    ADAPTER1_INIT = 'CTGTCTCTTATACACATCTCCGAGCCCACGAGAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1'

    ADAPTER2_INIT = 'CTGTCTCTTATACACATCTGACGCTGCCGACGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2'

    TRT_CONT_CHOICES = (('yes', _('yes')), ('no', _('no')))
    TRT_CONT_INIT = 'no'
    TRT_CONT_HELP = 'Run treatment sample versus control'
    TRT_CONT_LABEL = 'Run with control'


class CHIPseqConst(object):
    COMMANDS = [CUTADAPT, MACS2]
    ADAPTER1_INIT = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1'

    ADAPTER2_INIT = 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2'

    TRT_CONT_CHOICES = (('yes', _('yes')), ('no', _('no')))
    TRT_CONT_INIT = 'no'
    TRT_CONT_HELP = 'Run treatment sample versus control'
    TRT_CONT_LABEL = 'Run with control'


class RNAseqConst(object):
    COMMANDS = [CUTADAPT, Differential_gene_expression_analysis]
    # STRANDED_CHOICES = (('no', _('non stranded')), ('yes', _('stranded')), ('0.3', _('find automaticaly')))
    # STRANDED_HELP = 'The protocol is stranded or not. If find automaticaly is selected, the protocol will be determined according the the counts of the reads on the strands'
    STRANDED_CHOICES = (('no', _('non stranded')), ('yes', _('stranded')))
    STRANDED_HELP = 'The protocol is stranded or not.'
    STRANDED_LABEL = 'Stranded protocol'

    ADAPTER1_INIT = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    ADAPTER1_LABLE = 'Adapter on R1 (default: True-Seq kit)'

    ADAPTER2_INIT = 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    ADAPTER2_LABLE = 'Adapter on R2 (default: True-Seq kit)'

    DESEQRUN_CHOICES = (('yes', _('Run Deseq')), ('no', _('No Deseq')))
    DESEQRUN_INIT = 'no'
    DESEQRUN_HELP = 'Deseq is software for differential expression. You can run Deseq if you know the catergories of your samples (for example: knockout vs. control)'
    DESEQRUN_LABEL = 'Deseq run'


class MarsSeqConst(object):
    COMMANDS = [CUTADAPT, Differential_gene_expression_analysis]

class DESeqFromCountMatrixConst(object):
    COMMANDS = [Differential_gene_expression_analysis]

class AdvancedParameters(object):
    # CUTADAPT advancd parametres
    SELECT_ADAPTER1_INIT = ''  # the user have to enter the adapter
    SELECT_ADAPTER1_HELP = 'Adapter to remove from R1 read.'
    SELECT_ADAPTER1_LABLE = 'Additional adapter to remove from R1'

    SELECT_ADAPTER2_INIT = ''  # the user have to enter the adapter
    SELECT_ADAPTER2_HELP = 'Adapter to remove from R2 read.'
    SELECT_ADAPTER2_LABLE = 'Additional adapter to remove from R2'

    CUTADAPT_QVALUE_INIT = '20'
    CUTADAPT_QVALUE_INIT_ATAC = '25'
    CUTADAPT_QVALUE_HELP = '5\'CUTOFF,]3\'CUTOFF, --quality-cutoff=[5\'CUTOFF,]3\'CUTOFF Trim low-quality bases from 5\' and/or 3\' ends of each read before adapter removal. Applied to both reads \
if data is paired. If one value is given, only the 3\'end is trimmed. If two comma-separated cutoffs are given, the 5\' end is trimmed with the first cutoff, the 3\'end with the second.'
    CUTADAPT_QVALUE_LABEL = 'Quality-cutoff (-q)'

    CUTADAPT_MINIMUM_LENGTH_INIT = '25'
    CUTADAPT_MINIMUM_LENGTH_INIT_ATAC = '30'
    CUTADAPT_MINIMUM_LENGTH_HELP = '-m LENGTH, --minimum-length=LENGTH Discard reads shorter than LENGTH. Default: 0'
    CUTADAPT_MINIMUM_LENGTH_LABEL = 'Minimum reads length (-m)'

    CUTADAPT_CUT_READS_START_INIT = ''
    CUTADAPT_CUT_READS_START_MARSEQ_INIT = '3'
    CUTADAPT_CUT_READS_START_HELP = '-u LENGTH, --cut=LENGTH Remove bases from each read (first read only if paired). If LENGTH is positive, remove bases from the beginning. If LENGTH is negative, remove bases from the end. Can be used twice if LENGTHs have different signs. This is applied *before* adapter trimming.'
    CUTADAPT_CUT_READS_START_LABEL = 'Remove bases from read\'s start (-u)'

    CUTADAPT_CUT_READS_END_INIT = ''
    CUTADAPT_CUT_READS_END_MARSEQ_INIT = '-3'
    CUTADAPT_CUT_READS_END_HELP = '-u LENGTH, --cut=LENGTH Remove bases from each read (first read only if paired). If LENGTH is positive, remove bases from the beginning. If LENGTH is negative, remove bases from the end. Can be used twice if LENGTHs have different signs. This is applied *before* adapter trimming.'
    CUTADAPT_CUT_READS_END_LABEL = 'Remove bases from read\'s end (-u)'

    # DEseq adavced parameters

    FDR_CORRECTION_CHOICES = (('yes', _('correct with FDR tool')), ('no', _('dont correct with FDR tool')))
    FDR_CORRECTION_HELP = 'do you want to correct the data with FDR tool.'
    FDR_CORRECTION_LABEL = 'FDR tool correction'
    FDR_CORRECTION_INIT= 'no'

    LOG2FC_INIT = '1'
    LOG2FC_HELP = 'set the threshold for the log2 Fold Change value'
    LOG2FC_LABEL = 'Log2 Fold Change threshold'

    P_ADJ_INIT = '0.05'
    P_ADJ_HELP = 'set the threshold for the adjusted P value'
    P_ADJ_LABEL = 'Adjusted P-value'

    BASEMEAN_INIT = '5'
    BASEMEAN_HELP = 'set the threshold for the base mean'
    BASEMEAN_LABEL = 'Base mean'

    # MACS2 adavnced parameters
    MACS_QVALUE_INIT = '0.05'
    MACS_QVALUE_HELP = 'The qvalue (minimum FDR) cutoff to call significant regions. Default is 0.05. For broad marks, you can try 0.05 as cutoff. Q-values are calculated from p-values using Benjamini-Hochberg procedure.'
    MACS_QVALUE_LABEL = 'Q-value'

    MACS_BW_CHIP_INIT = '300'
    MACS_BW_ATAC_INIT = '120'
    MACS_BW_HELP = 'The bandwidth which is used to scan the genome ONLY for model building. Can be set to the expected fragment size.'
    MACS_BW_LABEL = 'Band-width'


class ReturnAllClassFields(object):
    def __init__(self):
        self.char_fields = []
        self.choise_fields = []
        self.float_fields = []
        self.integer_fields = []

    def get_char_fields(self):
        for name, form_field, required, initial, help_text, lable, widget in self.char_fields:
            yield name, form_field, required, initial, help_text, lable, widget

    def get_choise_fields(self):
        for name, form_field, required, initial, Choise, help_text, lable, widget in self.choise_fields:
            yield name, form_field, required, initial, Choise, help_text, lable, widget

    def get_float_fields(self):
        for name, form_field, required, initial, help_text, lable, widget in self.float_fields:
            yield name, form_field, required, initial, help_text, lable, widget

    def get_integer_fields(self):
        for name, form_field, required, initial, help_text, lable, widget in self.integer_fields:
            yield name, form_field, required, initial, help_text, lable, widget


class RNAseqFields(ReturnAllClassFields):
    def __init__(self):
        super(RNAseqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, True, RNAseqConst.ADAPTER1_INIT, RNAseqConst.ADAPTER1_HELP,
             RNAseqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, RNAseqConst.ADAPTER2_INIT, RNAseqConst.ADAPTER2_HELP,
             RNAseqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        self.choise_fields.extend([
            ['stranded', forms.ChoiceField, True, '', RNAseqConst.STRANDED_CHOICES, RNAseqConst.STRANDED_HELP,
             RNAseqConst.STRANDED_LABEL, forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})],
            ['correct_with_FDR_tool', forms.ChoiceField, True, AdvancedParameters.FDR_CORRECTION_INIT, AdvancedParameters.FDR_CORRECTION_CHOICES,
             AdvancedParameters.FDR_CORRECTION_HELP, AdvancedParameters.FDR_CORRECTION_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'command': Differential_gene_expression_analysis,
                                 'form_id': ADVANCED_ID})],
            ['deseq_run', forms.ChoiceField, True, RNAseqConst.DESEQRUN_INIT, RNAseqConst.DESEQRUN_CHOICES,
             RNAseqConst.DESEQRUN_HELP, RNAseqConst.DESEQRUN_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])


        self.float_fields.extend([
            ['log2_Fold_Change', forms.FloatField, False, AdvancedParameters.LOG2FC_INIT,
             AdvancedParameters.LOG2FC_HELP, AdvancedParameters.LOG2FC_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['adjusted_P_value', forms.FloatField, False, AdvancedParameters.P_ADJ_INIT,
             AdvancedParameters.P_ADJ_HELP, AdvancedParameters.P_ADJ_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['baseMean', forms.FloatField, False, AdvancedParameters.BASEMEAN_INIT,
             AdvancedParameters.BASEMEAN_HELP, AdvancedParameters.BASEMEAN_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])


class MarsSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(MarsSeqFields, self).__init__()
        self.char_fields.extend([
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE,
             forms.TextInput(
                 attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT,
                        'size': 80})]
        ])

        self.choise_fields.extend([
            ['correct_with_FDR_tool', forms.ChoiceField, True, AdvancedParameters.FDR_CORRECTION_INIT, AdvancedParameters.FDR_CORRECTION_CHOICES,
             AdvancedParameters.FDR_CORRECTION_HELP, AdvancedParameters.FDR_CORRECTION_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'command': Differential_gene_expression_analysis,
                                 'form_id': ADVANCED_ID})],
            ['deseq_run', forms.ChoiceField, True, RNAseqConst.DESEQRUN_INIT, RNAseqConst.DESEQRUN_CHOICES,
             RNAseqConst.DESEQRUN_HELP, RNAseqConst.DESEQRUN_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['log2_Fold_Change', forms.FloatField, False, AdvancedParameters.LOG2FC_INIT,
             AdvancedParameters.LOG2FC_HELP, AdvancedParameters.LOG2FC_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['adjusted_P_value', forms.FloatField, False, AdvancedParameters.P_ADJ_INIT,
             AdvancedParameters.P_ADJ_HELP, AdvancedParameters.P_ADJ_LABEL, forms.NumberInput(
                attrs={'class': 'btn btn-default', 'step': '0.01',
                       'command': Differential_gene_expression_analysis, 'form_id': ADVANCED_ID})],
            ['baseMean', forms.FloatField, False, AdvancedParameters.BASEMEAN_INIT,
             AdvancedParameters.BASEMEAN_HELP, AdvancedParameters.BASEMEAN_LABEL, forms.NumberInput(
                attrs={'class': 'btn btn-default', 'step': '0.01',
                       'command': Differential_gene_expression_analysis, 'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_MARSEQ_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_MARSEQ_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])


class ChipSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(ChipSeqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, False, CHIPseqConst.ADAPTER1_INIT, CHIPseqConst.ADAPTER1_HELP,
             CHIPseqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, CHIPseqConst.ADAPTER2_INIT, CHIPseqConst.ADAPTER2_HELP,
             CHIPseqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        self.choise_fields.extend([
            ['treat_vs_control', forms.ChoiceField, True, CHIPseqConst.TRT_CONT_INIT, CHIPseqConst.TRT_CONT_CHOICES,
             CHIPseqConst.TRT_CONT_HELP, CHIPseqConst.TRT_CONT_LABEL, forms.Select(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['macs_qvalue', forms.FloatField, False, AdvancedParameters.MACS_QVALUE_INIT,
             AdvancedParameters.MACS_QVALUE_HELP, AdvancedParameters.MACS_QVALUE_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'step': '0.01', 'command': MACS2,
                        'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['macs_bandwidth', forms.IntegerField, False, AdvancedParameters.MACS_BW_CHIP_INIT,
             AdvancedParameters.MACS_BW_HELP, AdvancedParameters.MACS_BW_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': MACS2,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_qvalue', forms.IntegerField, False, AdvancedParameters.CUTADAPT_QVALUE_INIT,
             AdvancedParameters.CUTADAPT_QVALUE_HELP, AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])


class AtacSeqFields(ReturnAllClassFields):
    def __init__(self):
        super(AtacSeqFields, self).__init__()
        self.char_fields.extend([
            ['adapter_on_R1', forms.CharField, True, ATACseqConst.ADAPTER1_INIT, ATACseqConst.ADAPTER1_HELP,
             ATACseqConst.ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['adapter_on_R2', forms.CharField, False, ATACseqConst.ADAPTER2_INIT, ATACseqConst.ADAPTER2_HELP,
             ATACseqConst.ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID, 'size': 80})],
            ['aditional_adapter_on_R1', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER1_INIT,
             AdvancedParameters.SELECT_ADAPTER1_HELP, AdvancedParameters.SELECT_ADAPTER1_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})],
            ['aditional_adapter_on_R2', forms.CharField, False, AdvancedParameters.SELECT_ADAPTER2_INIT,
             AdvancedParameters.SELECT_ADAPTER2_HELP, AdvancedParameters.SELECT_ADAPTER2_LABLE, forms.TextInput(
                attrs={'class': 'btn btn-default', 'form_id': ADVANCED_ID, 'command': CUTADAPT, 'size': 80})]
        ])

        self.choise_fields.extend([
            ['treat_vs_control', forms.ChoiceField, True, ATACseqConst.TRT_CONT_INIT, ATACseqConst.TRT_CONT_CHOICES,
             ATACseqConst.TRT_CONT_HELP, ATACseqConst.TRT_CONT_LABEL, forms.Select(
                attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['macs_qvalue', forms.FloatField, False, AdvancedParameters.MACS_QVALUE_INIT,
             AdvancedParameters.MACS_QVALUE_HELP, AdvancedParameters.MACS_QVALUE_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'step': '0.01', 'command': MACS2,
                        'form_id': ADVANCED_ID})]
        ])

        self.integer_fields.extend([
            ['macs_bandwidth', forms.IntegerField, False, AdvancedParameters.MACS_BW_ATAC_INIT,
             AdvancedParameters.MACS_BW_HELP, AdvancedParameters.MACS_BW_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': MACS2,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_qvalue', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_QVALUE_INIT_ATAC, AdvancedParameters.CUTADAPT_QVALUE_HELP,
             AdvancedParameters.CUTADAPT_QVALUE_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_min_len', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_INIT_ATAC,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_HELP,
             AdvancedParameters.CUTADAPT_MINIMUM_LENGTH_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_start', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_START_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_START_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_START_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '1', 'command': CUTADAPT,
                                      'form_id': ADVANCED_ID})],
            ['cutadapt_cut_end', forms.IntegerField, False,
             AdvancedParameters.CUTADAPT_CUT_READS_END_INIT,
             AdvancedParameters.CUTADAPT_CUT_READS_END_HELP,
             AdvancedParameters.CUTADAPT_CUT_READS_END_LABEL,
             forms.NumberInput(
                 attrs={'class': 'btn btn-default', 'max': '0', 'step': '1', 'command': CUTADAPT,
                        'form_id': ADVANCED_ID})]
        ])

class DESeqFromCountMatrixFields(ReturnAllClassFields):
    def __init__(self):
        super(DESeqFromCountMatrixFields, self).__init__()

        self.choise_fields.extend([
            ['correct_with_FDR_tool', forms.ChoiceField, True, AdvancedParameters.FDR_CORRECTION_INIT,
             AdvancedParameters.FDR_CORRECTION_CHOICES,
             AdvancedParameters.FDR_CORRECTION_HELP, AdvancedParameters.FDR_CORRECTION_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'command': Differential_gene_expression_analysis,
                                 'form_id': ADVANCED_ID})],
            ['deseq_run', forms.ChoiceField, True, RNAseqConst.DESEQRUN_INIT, RNAseqConst.DESEQRUN_CHOICES,
             RNAseqConst.DESEQRUN_HELP, RNAseqConst.DESEQRUN_LABEL,
             forms.Select(attrs={'class': 'btn btn-default', 'form_id': BASIC_ID})]
        ])

        self.float_fields.extend([
            ['log2_Fold_Change', forms.FloatField, False, AdvancedParameters.LOG2FC_INIT,
             AdvancedParameters.LOG2FC_HELP, AdvancedParameters.LOG2FC_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['adjusted_P_value', forms.FloatField, False, AdvancedParameters.P_ADJ_INIT,
             AdvancedParameters.P_ADJ_HELP, AdvancedParameters.P_ADJ_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})],
            ['baseMean', forms.FloatField, False, AdvancedParameters.BASEMEAN_INIT,
             AdvancedParameters.BASEMEAN_HELP, AdvancedParameters.BASEMEAN_LABEL,
             forms.NumberInput(attrs={'class': 'btn btn-default', 'step': '0.01',
                                      'command': Differential_gene_expression_analysis,
                                      'form_id': ADVANCED_ID})]
        ])


        # CATEGORY1_INIT = ''
        # CATEGORY1_HELP = 'fill the categories of the samples (for example: knockout vs. control). Use + button if there are more than 2 categories. Please use with a-z,A-Z,0-9 characters only (other characters will be converted to "_" character).'
        # CATEGORY1_LABLE = 'Categories'
        #
        # CATEGORY2_INIT = ''
        # CATEGORY2_LABEL = ''

        # class ReseqConst(object):
        #     ReseqAPPS =  [ CONSENSUS_APP,CORE_APP,ALIGNMENT_APP,REPORTS_APP ]
        #
        #     NO_SPLIT_SUBREADS_INIT = "False"
        #     NO_SPLIT_SUBREADS_CHOICES = ((False, _("No")), (True, _("Yes")))
        #     NO_SPLIT_SUBREADS_HELP = "Do not split reads into subreads even if subread "
        #     NO_SPLIT_SUBREADS_LABEL = "Align unsplit polymerase reads"
        #     ALGORITHM_CHOICES = (('best',_('best')), ('plurality', _('plurality')), ('arrow', _('arrow')))
        #     MIN_ACCURACY_INIT = 70.0
        #     MIN_ACCURACY_MIN = 0.0
        #
        #

        #####################  Import ######################

        # class BarcodingConst(object):
        #     Barcoding_APPS = [CORE_APP]
        #     BARCODES_TYPES = (('symmetric', _('symmetric')), ('asymmetric', _('asymmetric')))
        #
        #     SCORE_MODE_INIT = 'symmetric'
        #     SCORE_MODE_CHOICES = BARCODES_TYPES
        #     SCORE_MODE_HELP = 'The type of barcode sequence to use. asymmetric: Barcode sequences that are different on either end of an insert present in a SMRTbell template.'
        #     SCORE_MODE_LABEL = 'Option Score Mode'
        #
        #
