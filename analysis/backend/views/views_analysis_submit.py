"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import abc
import logging

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from analysis.backend.forms.forms_transcriptome import RnaSeqSubmitForm, MarsSeqSubmitForm
from analysis.backend.run.run_ngs_transcriptome_rnaseq import SubmitRnaSeq
from analysis.backend.run.run_ngs_transcriptome_marsseq import SubmitMarsSeq
from analysis.backend.utils.consts import RNASEQ_ANALYSIS_TYPE, MARSSEQ_ANALYSIS_TYPE, FUTURE_DATA_TYPE, RunStatus
from analysis.backend.utils.users import login_exists_user
from analysis.backend.views.views_analysis_base import AnalysisBaseView
from analysis.backend.views.views_general import LoginAsUser, logout_view
from analysis.models import CustomUser

logger = logging.getLogger(__name__)


class AnalysisSubmitView(AnalysisBaseView):
    @abc.abstractmethod
    def get_form_class(self, pipeline):
        if pipeline == 'Submit ' + RNASEQ_ANALYSIS_TYPE:
            return RnaSeqSubmitForm
        if pipeline == 'Submit ' + MARSSEQ_ANALYSIS_TYPE:
            return MarsSeqSubmitForm
        elif pipeline == 'Submit ' + FUTURE_DATA_TYPE:  # template for future analyses
            return 'FutureSubmitForm'

    @abc.abstractmethod
    def get_analysis_class(self, pipeline):
        if pipeline == 'Submit ' + RNASEQ_ANALYSIS_TYPE:
            return SubmitRnaSeq
        if pipeline == 'Submit ' + MARSSEQ_ANALYSIS_TYPE:
            return SubmitMarsSeq
        elif pipeline == 'Submit ' + FUTURE_DATA_TYPE:  # template for future analyses
            return 'SubmitFutureForm'

    @abc.abstractmethod
    def check_validation_specific_to_pipeline(self, analysis_form, request):
        self.check_validation(analysis_form, request, 'input_folder')

    @abc.abstractmethod
    def save_to_model_specific_to_pipeline(self, pipeline, analysis_form, request):
        if 'Submit' in pipeline:
            self.model_saved_.pipeline = pipeline.replace('Submit ', '')
            self.model_saved_.status = RunStatus.SUBMITTED
            # Send output folder from json file - No new output folder will be created
            self.output_dir_ = analysis_form.cleaned_data['output_folder']

    # Run without user (no login needed), but with user parameter in the url.
    def get(self, request, pipeline, *args, **kwargs):
        run_id = request.GET['run_id']
        username = request.GET['user']
        user = CustomUser.objects.get(username=username)
        if user.is_superuser:
            messages.error(request,
                           'You cannot submit analysis with superuser \"%s\". Please contact the administrator: utap@weizmann.ac.il' % request.user.username)
            logout_view(request)
            return HttpResponseRedirect(reverse('analysis:user_datasets'))
        login_exists_user('submit', request)
        LoginAsUser().login_as_user(username, request)
        kwargs.update({'run_id': run_id})
        return super(AnalysisSubmitView, self).get(request, pipeline, *args, **kwargs)

    # run by submit user (superuser) with json file
    def post(self, request, pipeline, *args, **kwargs):
        kwargs.update({'run_id': ''})
        return super(AnalysisSubmitView, self).post(request, pipeline, *args, **kwargs)
