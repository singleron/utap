# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2019-01-07 08:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0006_auto_20181219_1523'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='atacseqanalysis',
            options={'verbose_name_plural': 'ATAC-seq Analyses'},
        ),
        migrations.AlterField(
            model_name='analysis',
            name='pipeline',
            field=models.CharField(blank=True, choices=[(b'Transcriptome RNA-seq', 'Transcriptome RNA-seq'), (b'Transcriptome Mars-seq', 'Transcriptome Mars-seq'), (b'Demultiplexing_from_FASTQ', 'Demultiplexing_from_FASTQ'), (b'Demultiplexing_from_BCL', 'Demultiplexing_from_BCL'), (b'ATAC-seq', 'ATAC-seq'), (b'Demultiplexing_from_RUNID', 'Demultiplexing_from_RUNID'), (b'Singlecell Cellranger', 'Singlecell Cellranger'), (b'Transcriptome Mars-seq Deseq', 'Transcriptome Mars-seq Deseq'), (b'Transcriptome RNA-seq Deseq', 'Transcriptome RNA-seq Deseq')], max_length=200, verbose_name='Choose pipeline'),
        ),
    ]
