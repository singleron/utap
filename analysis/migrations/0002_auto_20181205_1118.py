# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-12-05 09:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BowtieGenomes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creature', models.CharField(max_length=200)),
                ('path', models.CharField(max_length=500)),
                ('alias', models.CharField(max_length=30)),
                ('version', models.CharField(max_length=10)),
            ],
            options={
                'verbose_name_plural': 'Bowtie Genomes',
            },
        ),
        migrations.AlterModelOptions(
            name='annotation',
            options={'verbose_name_plural': 'Annotations'},
        ),
        migrations.AlterModelOptions(
            name='cellrangergenomes',
            options={'verbose_name_plural': 'Cellranger Genomes'},
        ),
        migrations.AlterModelOptions(
            name='genome',
            options={'verbose_name_plural': 'Genomes'},
        ),
        migrations.AlterField(
            model_name='analysis',
            name='name',
            field=models.CharField(max_length=80, null=True, verbose_name='Project name'),
        ),
    ]
