"""
This file is part of UTAP.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import logging
import os
import urllib
import urllib2

from bs4 import BeautifulSoup

from analysis.backend.settings import JOBS_STATUS_FILE, NEXTSEQ_SERVER, NEXTSEQ_USER, FASTQ_REMOTE_PATH, FASTQ_REMOTE_PATH_INCPM, PIPELINE_URL
from analysis.backend.utils.consts import RunStatus
from analysis.backend.utils.emails import AnalysisEndedNotification, Notifier, FailedNotification
from analysis.backend.utils.errors import Errors
from analysis.backend.utils.general import open_remote_ssh_session
from analysis.backend.utils.users import get_user_dir
from analysis.backend.views.views_general import remote_samples_list
from analysis.models import Analysis
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger(__name__)


class TrackJobs(object):
    def __init__(self):
        pass

    @staticmethod
    def send_email(notification, email):
        Notifier(notification, email)

    @staticmethod
    def update_analysis_db():
        jobs_status = {}
        with open(JOBS_STATUS_FILE) as sf:
            for job_line in sf:
                job_name, status = job_line.split(' ')[-2], int(job_line.split(' ')[-1])
                status = RunStatus.SUCCESSFUL if status == 0 else RunStatus.FAILED
                jobs_status[job_name] = status
        success = []
        failed = []
        for job_name, status in jobs_status.items():
            try:
                obj = Analysis.objects.get(name=job_name)
            except ObjectDoesNotExist as e:
                continue
            if 'DELETED' in obj.status:
                continue
            old_status = obj.status
            obj.status = status
            user_email = obj.email
            if status == RunStatus.SUCCESSFUL and old_status == RunStatus.RUNNING:
                success.append(':'.join([obj.name, status]))
                obj.results = obj.results_temp
                TrackJobs.send_email(AnalysisEndedNotification(obj.pipeline, obj.name, obj.user.get_full_name(), obj.results, obj.parameters),
                                     user_email)
            if status != RunStatus.SUCCESSFUL and status != RunStatus.RUNNING and old_status == RunStatus.RUNNING:
                obj.results = "%s/run_failed/" % (PIPELINE_URL)
                failed.append(':'.join([obj.name, status]))
                date = "_".join(obj.name.split('_')[:2])
                output_folder = str(obj.output_folder)
                stderr = os.path.join(output_folder, "cluster_" + date + ".error")
                logs = os.path.join(output_folder, "logs_" + date)
                err = Errors(obj.name, output_folder)
                errors = err.all_err()
                TrackJobs.send_email(
                    FailedNotification(obj.pipeline, obj.name, obj.user.get_full_name(), stderr, logs, errors),
                    user_email)
            if old_status != status:  # save after assignment of obj.results = obj.results_temp
                obj.save()

        return success, failed


class RunSubmmited(object):
    def __init__(self):
        self.success = []
        self.failed = []

    def copy_fastq_directory(self, run_id, username):
        base_user_dir = get_user_dir(username)
        input_dir = os.path.join(base_user_dir, run_id + '_copied_from_stefan', run_id)
        samples_list = remote_samples_list(run_id, username, list_format=True)
        if not samples_list:
            message = 'Transcriptome analysis: copy fastq from stefan - no samples data in run %s' % run_id
            logger.error(message)
            return False

        os.system('mkdir -p %s' % input_dir)
        samples = samples_list + ['Undetermined']
        logger.info("Start to copy the files from stefan: %s" % (''.join(samples)))
        for sample in samples:
            logger.info('Transcriptome analysis: copy fastq file of sample: %s' % sample)
            fastq_remote_path = FASTQ_REMOTE_PATH_INCPM if ('NB501540' in run_id or 'A00929' in run_id) else FASTQ_REMOTE_PATH
            remote_fastq_dir = os.path.join(fastq_remote_path, run_id, sample)
            logger.info("rsync -av %s@%s:%s %s " % (NEXTSEQ_USER, NEXTSEQ_SERVER, remote_fastq_dir, input_dir))
            os.system("rsync -av %s@%s:%s %s " % (NEXTSEQ_USER, NEXTSEQ_SERVER, remote_fastq_dir, input_dir))
        logger.info('Transcriptome analysis: end to copy fastq files of run: %s' % run_id)
        return True

    def login_with_csrftoken(self):

        username = 'submit'
        password = 'sub12345'

        logger.info('Logging in %s' % username)
        logger.debug('Logging in %s' % username)

        # fetch the login page in order to get the csrf token
        cookieHandler = urllib2.HTTPCookieProcessor()
        opener = urllib2.build_opener(urllib2.HTTPHandler(), cookieHandler)
        urllib2.install_opener(opener)
 
        #must starts with http for urllib2 package
        #user_login must ending with "/" !!!! it is requirement of django to do POST with / in the end of url. see explanation on APPEND_SLASH=False argument
        login_url =  PIPELINE_URL + '/user_login/' if PIPELINE_URL.startswith('http') else 'http://' + PIPELINE_URL + '/user_login/'
        # login_url = PIPELINE_URL+'/submit_analysis/submit_Transcriptome%20Mars-seq/?user=testuser\&run_id=170507_NB501465_0098_AHYHW2BGXY'
        logger.debug("login_url: " + login_url)
        login_page = opener.open(login_url)

        # attempt to get the csrf token from the cookie jar
        csrf_cookie = None
        for cookie in cookieHandler.cookiejar:
            if cookie.name == 'csrftoken':
                csrf_cookie = cookie
                break
        if not csrf_cookie:
            logger.error("No csrf cookie found")
            raise IOError("No csrf cookie found")
        logger.info("found csrf cookie: " + str(csrf_cookie))
        logger.info("csrf_token = %s" % csrf_cookie.value)

        # login using the usr, pwd, and csrf token
        login_data_dict = dict(username=username, password=password, csrfmiddlewaretoken=csrf_cookie.value)
        login_data = urllib.urlencode(login_data_dict)
        logger.info("login_data: %s" % login_data)

        req = urllib2.Request(login_url, login_data)
        response = urllib2.urlopen(req)
        # <--- 403: FORBIDDEN here

        logger.debug('response url:\n' + str(response.geturl()) + '\n')
        logger.debug('response info:\n' + str(response.info()) + '\n')

        # should redirect to the welcome page here, if back at log in - refused
        if response.geturl() == login_url:
            logger.error('Authentication refused')
            raise IOError('Authentication refused')

        logger.info('\t%s is logged in' % username)
        # save the cookies/opener for further actions

        return login_data_dict, opener

    def run_submitted(self, params_run, obj):
        pipeline = obj.pipeline.replace(' ', '%20')
        run_id = params_run['run_id']
        user = params_run['user']
        del params_run['user']
        obj.status = RunStatus.COPY
        obj.save()
        response = self.copy_fastq_directory(run_id, user)
        if not response:
            self.failed.append(':'.join([run_id, 'The copied of the run: %s is failed' % run_id]))
            obj.status = RunStatus.FAILED
            obj.save()
        logger.info("Start run %s analysis of %s" % (pipeline, run_id))
        
        #must starts with http for urllib2 package
        pipeline_url =  PIPELINE_URL if PIPELINE_URL.startswith('http') else 'http://' + PIPELINE_URL
        url = pipeline_url + '/analysis/%s/?user=%s' % (pipeline, user)
        self.login_with_csrftoken()
        data = urllib.urlencode(params_run)

        request = urllib2.Request(url=url, data=data)
        # request.add_header("Content-Type", 'application/json')
        try:
            response = urllib2.urlopen(request, timeout=1000000000)
            # response = urllib2.urlopen(url, data, timeout=1000000000)
            content = response.read()
            if 'alert alert-danger' in content:
                soup = BeautifulSoup(content, "html.parser")
                rows = soup.find_all('div', attrs={"class": "alert alert-danger"})
                msg = 'POST of submitted run: %s %s' % (params_run['run_id'], rows[0].text)
                logger.debug(msg)
                self.failed.append(':'.join([run_id, msg]))
                return False
        except urllib2.HTTPError, e:
            response = e

        if response.code == 200:
            data = response.read()
            self.success.append(':'.join([run_id, '']))
            return True
        else:
            msg = 'error in connection ' + str(response) + " __ " + str(response.code)
            logger.error(msg)
            self.failed.append(':'.join([run_id, msg]))
            return False

    def runs_ended(self, submitted_run_id, params_runs, submitted_objs):
        session = open_remote_ssh_session(server_name=NEXTSEQ_SERVER, server_user=NEXTSEQ_USER, server_pass='')
        params_files_ended = []
        submitted_objs_ended = []
        for run_id, params_run, submitted_obj in zip(submitted_run_id, params_runs, submitted_objs):
            fastq_remote_path = FASTQ_REMOTE_PATH_INCPM if ('NB501540' in run_id or 'A00929' in run_id) else FASTQ_REMOTE_PATH
            status_run_file = os.path.join(fastq_remote_path, run_id, 'dataDistributionStatus.xml')
            cmd = "grep CompletedAsPlanned %s" % status_run_file
            ssh_stdin, ssh_stdout, ssh_stderr = session.exec_command(cmd)
            stdout = ssh_stdout.read()
            if ssh_stderr.read():
                self.failed.append(':'.join([run_id, ssh_stderr.read()]))

            print ssh_stderr.read()
            if stdout and 'CompletedAsPlanned' in stdout:
                params_files_ended.append(params_run)
                submitted_objs_ended.append(submitted_obj)
        session.close()
        return params_files_ended, submitted_objs_ended

    def run_submitted_analyses(self):
        submitted_objs = Analysis.objects.filter(status=RunStatus.SUBMITTED)
        submitted_run_id = []
        params_runs = []
        for submitted_obj in submitted_objs:
            params_run_file = submitted_obj.parameters_json
            if not os.path.isfile(params_run_file):
                continue
            with open(params_run_file) as f:
                params_raw_content = f.read()
            params_run = json.JSONDecoder().decode(params_raw_content)
            submitted_run_id.append(params_run['run_id'])
            params_runs.append(params_run)

        params_runs_ended, submitted_objs_ended = self.runs_ended(submitted_run_id, params_runs, submitted_objs)
        for params_run_ended, submitted_obj in zip(params_runs_ended, submitted_objs_ended):
            status = self.run_submitted(params_run_ended, submitted_obj)
            if status:
                submitted_obj.delete()
        return self.success, self.failed
