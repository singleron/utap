#save the old db, and create new db by run:
cp db.sqlite3 db.sqlite3.old

rm db.sqlite3
rm -rf analysis/migrations
python manage.py makemigrations analysis
python manage.py migrate
===========
#migrate the old db content into the new db:

sqlite3 db.sqlite3

ATTACH DATABASE 'db.sqlite3.old' AS olddb;

INSERT OR REPLACE INTO django_session SELECT * FROM olddb.django_session;
INSERT OR REPLACE INTO django_migrations SELECT * FROM olddb.django_migrations;
INSERT OR REPLACE INTO django_content_type SELECT * FROM olddb.django_content_type;
INSERT OR REPLACE INTO django_admin_log SELECT * FROM olddb.django_admin_log;
INSERT OR REPLACE INTO auth_permission SELECT * FROM olddb.auth_permission;
INSERT OR REPLACE INTO auth_group_permissions SELECT * FROM olddb.auth_group_permissions;
INSERT OR REPLACE INTO auth_group SELECT * FROM olddb.auth_group;
#INSERT INTO analysis_profile SELECT * FROM olddb.analysis_profile; #no such table in old db
INSERT INTO analysis_genome SELECT * FROM olddb.analysis_genome;
INSERT INTO analysis_customuser_user_permissions SELECT * FROM olddb.analysis_customuser_user_permissions;
INSERT INTO analysis_customuser_groups SELECT * FROM olddb.analysis_customuser_groups;
INSERT INTO analysis_customuser SELECT * FROM olddb.analysis_customuser;
INSERT INTO analysis_annotation SELECT * FROM olddb.analysis_annotation;


INSERT INTO analysis_analysis(id, name,output_folder,parameters,parameters_json,date,user_id,user_run,pipeline,status,results,results_temp,email) SELECT id, name,output_folder,parameters,parameters_json,date,user_id,user_run,pipeline,status,results,results_temp,email FROM olddb.analysis_analysis;
INSERT INTO analysis_transcriptomanalysis(analysis_ptr_id, input_folder,genome_id,annotation_id) SELECT id,input_folder,genome_id,annotation_id FROM olddb.analysis_analysis WHERE pipeline IN ('Transcriptome RNA-seq', 'Transcriptome Mars-seq', 'Transcriptome Mars-seq Deseq', 'Transcriptome RNA-seq Deseq');
INSERT INTO analysis_transcriptomsubmitanalysis(transcriptomanalysis_ptr_id,run_id) SELECT id,run_id FROM olddb.analysis_analysis WHERE pipeline IN ('Transcriptome RNA-seq', 'Transcriptome Mars-seq', 'Transcriptome Mars-seq Deseq', 'Transcriptome RNA-seq Deseq') AND run_id IS NOT NULL;
INSERT INTO analysis_demultiplexinganalysis(analysis_ptr_id,bcl_files,fastq_file_R1,fastq_file_R2,fastq_file_I,run_id) SELECT id,bcl_files,fastq_file_R1,fastq_file_R2,fastq_file_I,run_id FROM olddb.analysis_analysis WHERE pipeline IN ('Demultiplexing_from_RUNID', 'Demultiplexing_from_FASTQ', 'Demultiplexing_from_BCL');


#Manually replace the logged_in_as_path with logged_in_as_user, for all superusers - it is because the ids of these fields were exchange in the new table



