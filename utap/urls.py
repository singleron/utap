"""utap URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views
from django.contrib.auth import views as auth_views
from filebrowser.sites import site

from analysis.backend.forms.forms_base import LoginForm
from analysis.backend.settings import BBCU_INTERNAL, INSTITUTE_NAME, DEMO_SITE, UTAP_VERSION

urlpatterns = [
    url(r'', include('analysis.backend.urls')),
    url('^', include('django.contrib.auth.urls')),
    # url(r'^flowjs/', include('flowjs.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/login/$', views.login, {'template_name': 'login.html', 'authentication_form': LoginForm},
        name='login'),
    url(r'^user_login/$', views.login,
        {'template_name': 'login.html', 'extra_context': {'bbcu_internal': BBCU_INTERNAL, 'institute_name': INSTITUTE_NAME, 'demosite': DEMO_SITE, 'version': UTAP_VERSION}}, name='user_login'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
]
